package com.coffeeteam.coffeeviet;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.coffeeteam.coffeeviet.entities.GooglePlaceWebServiceGeometry;
import com.coffeeteam.coffeeviet.entities.Location;
import com.coffeeteam.coffeeviet.managers.networks.LocationRequestFromAddressAsync;
import com.coffeeteam.coffeeviet.utils.JsonParserUtil;
import com.coffeeteam.coffeeviet.utils.StringFormatUtil;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    private static final LatLng FAILED_REQUEST_RESULT = new LatLng(0.0D, 0.0D);
    @Mock
    private Context context;

    @Mock
    private Geocoder geocoder;

    private static final String TAG = ExampleUnitTest.class.getSimpleName();

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void checkOpeningTimeValidation() throws ParseException {
        String openingTime = "6:00";
        String closingTime = "11:00";
        assertFalse(StringFormatUtil.isOpeningTime(openingTime, closingTime));
    }


    @Test
    public void testGetLocationFromAddressString() {
        String address = "Kí túc xá khu B"; // get address for requesting
        LatLng returnResult = null; // value to be returned
        //geocoder = new Geocoder(this.context);
        List<Address> listOfAddress;
        try {
            listOfAddress = geocoder.getFromLocationName(address, 2); // set max result to 2
            // make sure request results is valid
            if (listOfAddress == null || listOfAddress.isEmpty()) {
                returnResult = FAILED_REQUEST_RESULT;
            }

            Address firstResult = listOfAddress.get(0); // get first result;
            returnResult = new LatLng(firstResult.getLatitude(), firstResult.getLongitude());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Error with message : " + e.getMessage(), e);
        }

        Log.d(TAG, returnResult.toString());
    }

}