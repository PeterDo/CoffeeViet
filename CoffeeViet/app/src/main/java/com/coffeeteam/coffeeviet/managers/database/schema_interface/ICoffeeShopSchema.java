package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 17/03/2017.
 */

public interface ICoffeeShopSchema {
    public static final String SHOP_TABLE = "COFFEE_SHOP";

    public static final String ID = "_id";
    public static final String SHOP_ID = "id";
    public static final String SHOP_NAME = "name";
    public static final String SHOP_ADDRESS = "address";
    public static final String SHOP_QUOTE = "quote";
    public static final String SHOP_OWNER_ID = "owner_id";
    public static final String SHOP_LIKE = "like";
    public static final String SHOP_SHARE = "share";
    public static final String SHOP_OPENING_TIME = "opening_time";
    public static final String SHOP_CLOSING_TIME = "closing_time";
    public static final String SHOP_CREATION_TIME = "creation_time";
    public static final String SHOP_ACTIVE = "active";
    public static final String SHOP_RATING = "rating";

    public static final String SHOP_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + SHOP_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + SHOP_ID
            + " TEXT NOT NULL, "
            + SHOP_NAME
            + " TEXT NOT NULL, "
            + SHOP_ADDRESS
            + " TEXT,"
            + SHOP_QUOTE
            + " TEXT, "
            + SHOP_OWNER_ID
            + " TEXT NOT NULL, "
            + SHOP_LIKE
            + " INT, "
            + SHOP_SHARE
            + " INT, "
            + SHOP_OPENING_TIME
            + " DATE, "
            + SHOP_CLOSING_TIME
            + " DATE, "
            + SHOP_CREATION_TIME
            + " DATE, "
            + SHOP_ACTIVE
            + " BOOLEAN, "
            + SHOP_RATING
            + " INT"
            + ")";

    public static final String[] SHOP_COLUMNS = new String[]{ID,
            SHOP_ID, SHOP_NAME, SHOP_ADDRESS, SHOP_QUOTE, SHOP_OWNER_ID, SHOP_LIKE,
            SHOP_SHARE, SHOP_OPENING_TIME, SHOP_CLOSING_TIME, SHOP_CREATION_TIME,
            SHOP_ACTIVE, SHOP_RATING};
}
