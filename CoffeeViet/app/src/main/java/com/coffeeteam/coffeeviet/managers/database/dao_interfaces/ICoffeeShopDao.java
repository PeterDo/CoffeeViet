package com.coffeeteam.coffeeviet.managers.database.dao_interfaces;

import com.coffeeteam.coffeeviet.entities.CoffeeShop;

import java.util.List;

/**
 * Created by Hau-Do on 18/03/2017.
 */

public interface ICoffeeShopDao {
    public CoffeeShop fetchCoffeeShopById(int shopId);

    public List<CoffeeShop> fetchAllCoffeeShops();

    public boolean addCoffeeShop(CoffeeShop coffeeShop);

    public boolean addCoffeeShop(List<CoffeeShop> coffeeShops);

    public boolean deleteAllCoffeeShops();
}
