package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.ICoffeeShopDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.ICoffeeShopSchema;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hau-Do on 18/03/2017.
 */

public class CoffeeShopDao extends DbContentProvider implements ICoffeeShopSchema, ICoffeeShopDao {

    private Cursor mCursor;
    private List<CoffeeShop> mCoffeeShopList;
    private ContentValues mInitialValues;

    public CoffeeShopDao(SQLiteDatabase db) {
        super(db);
    }


    @Override
    protected CoffeeShop cursorToEntity(Cursor cursor) {
        CoffeeShop coffeeShop = new CoffeeShop();

        int idIndex;
        int shopIdIndex;
        int nameIndex;
        int addressIndex;
        int quoteIndex;
        int ownerIdIndex;
        int likesIndex;
        int sharesIndex;
        int openingTimeIndex;
        int closingTimeIndex;
        int creationTimeIndex;
        int isActiveIndex;
        int ratingIndex;

        if (cursor != null) {
//            if (cursor.getColumnIndex(ID) != -1) {
//                idIndex = cursor.getColumnIndexOrThrow(ID);
//                coffeeShop.setId(cursor.getInt(idIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_ID) != -1) {
//                shopIdIndex = cursor.getColumnIndexOrThrow(SHOP_ID);
//                coffeeShop.setShopId(cursor.getString(shopIdIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_NAME) != -1) {
//                nameIndex = cursor.getColumnIndexOrThrow(SHOP_NAME);
//                coffeeShop.setName(cursor.getString(nameIndex));
//            }
////            if (cursor.getColumnIndex(SHOP_ADDRESS) != -1) {
////                addressIndex = cursor.getColumnIndexOrThrow(SHOP_ADDRESS);
////                coffeeShop.setAddress(cursor.getString(addressIndex));
////            }
//            if (cursor.getColumnIndex(SHOP_QUOTE) != -1) {
//                quoteIndex = cursor.getColumnIndexOrThrow(SHOP_QUOTE);
//                coffeeShop.setQuote(cursor.getString(quoteIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_OWNER_ID) != -1) {
//                ownerIdIndex = cursor.getColumnIndexOrThrow(SHOP_OWNER_ID);
//                coffeeShop.setOwnerId(cursor.getString(ownerIdIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_LIKE) != -1) {
//                likesIndex = cursor.getColumnIndexOrThrow(SHOP_LIKE);
//                coffeeShop.setLikes(cursor.getLong(likesIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_SHARE) != -1) {
//                sharesIndex = cursor.getColumnIndexOrThrow(SHOP_SHARE);
//                coffeeShop.setShares(cursor.getLong(sharesIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_OPENING_TIME) != -1) {
//                openingTimeIndex = cursor.getColumnIndexOrThrow(SHOP_OPENING_TIME);
//                coffeeShop.setOpeningTime(cursor.getString(openingTimeIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_CLOSING_TIME) != -1) {
//                closingTimeIndex = cursor.getColumnIndexOrThrow(SHOP_CLOSING_TIME);
//                coffeeShop.setClosingTime(cursor.getString(closingTimeIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_CREATION_TIME) != -1) {
//                creationTimeIndex = cursor.getColumnIndexOrThrow(SHOP_CREATION_TIME);
//                coffeeShop.setCreationTime(cursor.getString(creationTimeIndex));
//            }
//            if (cursor.getColumnIndex(SHOP_ACTIVE) != -1) {
//                isActiveIndex = cursor.getColumnIndexOrThrow(SHOP_ACTIVE);
//                coffeeShop.setActive(cursor.getInt(isActiveIndex) > 0);
//            }
//            if (cursor.getColumnIndex(SHOP_RATING) != -1) {
//                ratingIndex = cursor.getColumnIndexOrThrow(SHOP_RATING);
//                coffeeShop.setRating(cursor.getInt(ratingIndex));
//            }
        }
        return coffeeShop;
    }

    @Override
    public CoffeeShop fetchCoffeeShopById(int shopId) {
        final String selectionArgs[] = {String.valueOf(shopId)};
        final String selection = SHOP_ID + " = ?";
        CoffeeShop coffeeShop = new CoffeeShop();
        mCursor = super.query(SHOP_TABLE, SHOP_COLUMNS, selection,
                selectionArgs, SHOP_NAME);
        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                coffeeShop = cursorToEntity(mCursor);
                mCursor.moveToNext();
            }
            mCursor.close();
        }

        return coffeeShop;
    }

    @Override
    public List<CoffeeShop> fetchAllCoffeeShops() {
        mCoffeeShopList = new ArrayList<CoffeeShop>();
        mCursor = super.query(SHOP_TABLE, SHOP_COLUMNS, null,
                null, ID);

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                CoffeeShop coffeeShop = cursorToEntity(mCursor);
                mCoffeeShopList.add(coffeeShop);
                mCursor.moveToNext();
            }
            mCursor.close();
        }

        return mCoffeeShopList;
    }

    @Override
    public boolean addCoffeeShop(CoffeeShop coffeeShop) {
        setContentValue(coffeeShop);
        try {
            return super.insert(SHOP_TABLE, getContentValue()) > 0;
        } catch (SQLiteConstraintException ex) {
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean addCoffeeShop(List<CoffeeShop> coffeeShops) {
        try {
            mDb.beginTransaction();
            for (CoffeeShop coffeeShop : coffeeShops) {
                addCoffeeShop(coffeeShop);
            }
            mDb.setTransactionSuccessful();
        } finally {
            mDb.endTransaction();
        }
        return true;
    }

    @Override
    public boolean deleteAllCoffeeShops() {
        return super.delete(SHOP_TABLE, null, null) > 0;
    }

    private ContentValues getContentValue() {
        return mInitialValues;
    }

    private void setContentValue(CoffeeShop coffeeShop) {
//        mInitialValues = new ContentValues();
//        mInitialValues.put(SHOP_ID, coffeeShop.getShopId());
//        mInitialValues.put(SHOP_NAME, coffeeShop.getName());
//        /**
//         * finding a proper solution for Location object (with key: SHOP_ADDRESS)
//         */
//        mInitialValues.put(SHOP_ADDRESS, coffeeShop.getAddress().getLatitude());
//        mInitialValues.put(SHOP_ADDRESS, coffeeShop.getAddress().getLongitude());
//        mInitialValues.put(SHOP_QUOTE, coffeeShop.getQuote());
//        mInitialValues.put(SHOP_OWNER_ID, coffeeShop.getOwnerId());
//        mInitialValues.put(SHOP_LIKE, coffeeShop.getLikes());
//        mInitialValues.put(SHOP_SHARE, coffeeShop.getShares());
//        mInitialValues.put(SHOP_OPENING_TIME, coffeeShop.getOpeningTime());
//        mInitialValues.put(SHOP_CLOSING_TIME, coffeeShop.getClosingTime());
//        mInitialValues.put(SHOP_CREATION_TIME, coffeeShop.getCreationTime());
//        mInitialValues.put(SHOP_ACTIVE, coffeeShop.isActive());
//        mInitialValues.put(SHOP_RATING, coffeeShop.getRating());
    }
}
