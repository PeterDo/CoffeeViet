package com.coffeeteam.coffeeviet.entities;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by TNS on 6/5/2017.
 * Represent a step information in a waypoints
 */

public class GoogleDirectionStep {
    private String distanceString;
    private String durationString;
    private LatLng endLocation;
    private LatLng startLocation;
    private String htmlInstruction;
    private String polylinePoint;


    public String getDistanceString() {
        return distanceString;
    }

    public void setDistanceString(String distanceString) {
        this.distanceString = distanceString;
    }

    public String getDurationString() {
        return durationString;
    }

    public void setDurationString(String durationString) {
        this.durationString = durationString;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public String getHtmlInstruction() {
        return htmlInstruction;
    }

    public void setHtmlInstruction(String htmlInstruction) {
        this.htmlInstruction = htmlInstruction;
    }

    public String getPolylinePoint() {
        return polylinePoint;
    }

    public void setPolylinePoint(String polylinePoint) {
        this.polylinePoint = polylinePoint;
    }
}
