package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.coffeeteam.coffeeviet.entities.FriendRelationship;
import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.IFriendRelationshipDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.IFriendRelationshipSchema;

import java.util.List;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public class FriendRelationshipDao extends DbContentProvider implements IFriendRelationshipSchema, IFriendRelationshipDao {

    private Cursor mCursor;
    private List<FriendRelationship> mRelationshipList;
    private ContentValues mInitialValues;

    public FriendRelationshipDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    protected <T> T cursorToEntity(Cursor cursor) {
        return null;
    }
}
