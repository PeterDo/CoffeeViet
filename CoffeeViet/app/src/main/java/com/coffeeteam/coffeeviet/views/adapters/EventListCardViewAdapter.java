package com.coffeeteam.coffeeviet.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.Event;
import com.coffeeteam.coffeeviet.managers.activities.EventDetailActivity;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;

import java.util.List;

/**
 * Created by TNS on 4/1/2017.
 */

public class EventListCardViewAdapter
        extends RecyclerView.Adapter<EventListCardViewAdapter.ViewHolder> {

    private List<Event> listOfEvents;
    private Context context;


    public EventListCardViewAdapter(Context context, List<Event> events) {
        this.context = context;
        this.listOfEvents = events;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coffee_shop_event_list_item, parent, false);

        return new EventListCardViewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // to update code here
        final Event event = this.listOfEvents.get(position);
        holder.textViewEventName.setText(event.getName());
        holder.textViewEventDescription.setText(event.getDescription());
        holder.textViewEventTime.setText(event.getStart_date());

        holder.textViewEventDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEventDetailIntent(event);
            }
        });

        holder.textViewEventName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEventDetailIntent(event);
            }
        });
    }

    private void moveToEventDetailIntent(Event event) {
        Intent eventDetailIntent = new Intent(this.context, EventDetailActivity.class);
        if (event != null) {
            eventDetailIntent.putExtra(PrefsUtil.SELECTED_EVENT, event);
        }
        this.context.startActivity(eventDetailIntent);
    }

    @Override
    public int getItemCount() {
        return this.listOfEvents.size();
    }

    /**
     * Provide a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewEventName;
        public TextView textViewEventTime;
        public TextView textViewEventDescription;

        public ViewHolder(View view) {
            super(view);
            // start to bind view by id
            this.textViewEventName = (TextView) view.findViewById(R.id.textview_item_even_name);
            this.textViewEventTime = (TextView) view.findViewById(R.id.textview_item_event_time);
            this.textViewEventDescription = (TextView) view.findViewById(R.id.textview_item_event_description);
        }
    }

}
