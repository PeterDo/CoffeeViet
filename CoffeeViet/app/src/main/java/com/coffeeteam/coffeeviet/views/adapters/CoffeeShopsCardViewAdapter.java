package com.coffeeteam.coffeeviet.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.managers.activities.CoffeeShopDetail;
import com.coffeeteam.coffeeviet.utils.CardViewAnimationUtil;
import com.coffeeteam.coffeeviet.utils.CustomAnimationUtil;
import com.coffeeteam.coffeeviet.utils.DateTimeUtil;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;
import com.coffeeteam.coffeeviet.views.view_interfaces.ViewHolderFieldAccess;
import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.List;

/**
 * RecycleView Adapter for coffees shops screen
 * Created by TNS on 3/15/2017.
 */
public class CoffeeShopsCardViewAdapter extends
        RecyclerView.Adapter<CoffeeShopsCardViewAdapter.ViewHolder> {

    private static final String TAG = CoffeeShopsCardViewAdapter.class.getSimpleName(); // tag for logging

    private Context context; // context of adapter
    private List<CoffeeShop> listOfCoffeeShops;
    private int lastAnimationViewHolderIndex = 0; // default to 0

    private DatabaseReference databaseReference;


    public CoffeeShopsCardViewAdapter(Context context,
                                      List<CoffeeShop> listOfCoffeeShops) {
        this.context = context;
        this.listOfCoffeeShops = listOfCoffeeShops;
    }

    /**
     * @param context
     * @param databaseRef
     */
    public CoffeeShopsCardViewAdapter(Context context, DatabaseReference databaseRef) {
        this.context = context;
        this.databaseReference = databaseRef;
    }


    /**
     * Bind view layout from resource
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coffee_shop_information_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // get coffee shop at index is position as second parameter
        //CoffeeShop currentCoffeeShop = this.listOfCoffeeShops.get(position);
        // start to set up view values for ViewHolder
        final CoffeeShop currentCoffeeShop = this.listOfCoffeeShops.get(position);
        // make sure we have valid element
        if (currentCoffeeShop != null) {
            holder.textViewAddress.setText(currentCoffeeShop.getAddress() + " - " +
                    currentCoffeeShop.getLikeCount() + " " + this.context.getString(R.string.like));
            holder.textViewCoffeeShopName.setText(currentCoffeeShop.getName());
            holder.textViewQuickComment.setText(currentCoffeeShop.getDescription());
            // detect if this is new created coffee shop
            try {
                if (DateTimeUtil.isNewShop(currentCoffeeShop.getCreating_time())) {
                    holder.textViewIsNewShop.setVisibility(View.VISIBLE);
                } else {
                    holder.textViewIsNewShop.setVisibility(View.INVISIBLE);
                }
            } catch (ParseException e) {
                Log.d(TAG, e.getMessage(), e);
                e.printStackTrace();
            }
//            Log.d(TAG, currentCoffeeShop.getPhotoUrl());
            Picasso.with(context)
                    .load(currentCoffeeShop.getPhotoUrl())
                    .placeholder(R.color.icon_color)
                    .fit() // fit size of image view
                    .centerCrop()
                    .into(holder.imageViewCoffeeShop, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });

            // image click listener
            holder.imageViewCoffeeShop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent coffeeShopDetailIntent = new Intent(context, CoffeeShopDetail.class);
                    Bundle bundle = new Bundle();
                    // put bundle
                    bundle.putParcelable(PrefsUtil.CURRENT_SHOP, currentCoffeeShop);
                    coffeeShopDetailIntent.putExtras(bundle);
                    context.startActivity(coffeeShopDetailIntent);
                }
            });
            CardViewAnimationUtil.flipOnTouchCardAnimation(context, holder);
            if (position >= this.lastAnimationViewHolderIndex) {
                // start card view appearance animation
                CardViewAnimationUtil.animateCardViewAppearance(context, holder, position);
                this.lastAnimationViewHolderIndex = position; // save last position of card view animation
            }

        }

    }

    @Override
    public int getItemCount() {
        return this.listOfCoffeeShops.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends
            RecyclerView.ViewHolder implements ViewHolderFieldAccess {
        // each data item will contain coffee shop information
        public CardView cardView;
        public ImageView imageViewCoffeeShop;
        public TextView textViewCoffeeShopName;
        public TextView textViewAddress;
        public TextView textViewQuickComment;
        public TextView textViewIsNewShop;

        // image buttons
        public ImageButton imageButtonFavorite;
        public ImageButton imageButtonShare;

        // flag for enable animation or not
        public boolean shouldBeAnimated;

        // member constructor with parameter is view containing all child elements
        public ViewHolder(View view) {
            super(view);
            this.cardView = (CardView) view.findViewById(R.id.cardview_coffeeshop_detail_item);
            this.imageViewCoffeeShop = (ImageView) view.findViewById(R.id.imageview_coffee_shop);
            this.textViewCoffeeShopName = (TextView) view.findViewById(R.id.textview_coffee_shop_name);
            this.textViewAddress = (TextView) view.findViewById(R.id.textview_coffee_shop_address);
            this.textViewQuickComment = (TextView) view.findViewById(R.id.textview_coffee_shop_quick_comment);
            this.textViewIsNewShop = (TextView) view.findViewById(R.id.textview_newCoffeeShop);
            this.imageButtonFavorite = (ImageButton) view.findViewById(R.id.image_button_favorite);
            this.imageButtonFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CustomAnimationUtil.favoriteIconAnimate(imageButtonFavorite);
                }
            });
            this.imageButtonShare = (ImageButton) view.findViewById(R.id.image_button_share);
            this.shouldBeAnimated = true; // default if true
        }

        @Override
        public CardView getCardView() {
            return this.cardView;
        }

        @Override
        public boolean getShouldAnimateValue() {
            return this.shouldBeAnimated;
        }

        @Override
        public void shouldAnimate(boolean value) {
            this.shouldBeAnimated = value;
        }
    }
}
