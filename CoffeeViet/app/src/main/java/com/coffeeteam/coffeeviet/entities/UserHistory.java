package com.coffeeteam.coffeeviet.entities;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class UserHistory {

    private int _id;
    private String id;
    private String user_id;
    private String coffee_shop_id;
    private String even_id;
    private String history_time;
    private String description;
    private boolean is_active;

    public UserHistory() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCoffee_shop_id() {
        return coffee_shop_id;
    }

    public void setCoffee_shop_id(String coffee_shop_id) {
        this.coffee_shop_id = coffee_shop_id;
    }

    public String getEven_id() {
        return even_id;
    }

    public void setEven_id(String even_id) {
        this.even_id = even_id;
    }

    public String getHistory_time() {
        return history_time;
    }

    public void setHistory_time(String history_time) {
        this.history_time = history_time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean is_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }
}
