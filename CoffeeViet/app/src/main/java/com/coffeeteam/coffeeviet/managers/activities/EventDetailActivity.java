package com.coffeeteam.coffeeviet.managers.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.Event;
import com.coffeeteam.coffeeviet.utils.DateTimeUtil;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;

import java.text.ParseException;


/**
 * Show detail of specified event
 */
public class EventDetailActivity extends AppCompatActivity {

    private static final String TAG = EventDetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        // get Parcelable value
        Event event = this.getIntent().getParcelableExtra(PrefsUtil.SELECTED_EVENT);
        if (event != null) {
            Log.d(TAG, "selected event infor : " + event.toString());
            this.prepareViewContent(event);
        }

    }

    /**
     *
     */
    private void prepareViewContent(Event event) {
        TextView textViewEventName = (TextView) this.findViewById(R.id.textView_detail_event_name);
        textViewEventName.setText(event.getName());
        TextView textViewEventDescription = (TextView) this.findViewById(R.id.textview_detail_event_description);
        textViewEventDescription.setText(event.getDescription());
        TextView textViewEventStartTime = (TextView) this.findViewById(R.id.textview_detail_event_time);
        textViewEventStartTime.setText(event.getStart_date() + " - " + event.getStart_time());

        if (!event.getStart_date().isEmpty()) {
            try {
                TextView textViewEventInterval = (TextView) this.findViewById(R.id.textview_detail_event_interval);
                if (DateTimeUtil.getDateDiff(event.getStart_date()) < 0) { // the even is over
                    textViewEventInterval.setText(R.string.event_over);
                } else if (DateTimeUtil.getDateDiff(event.getStart_date()) == 0) { // today
                    textViewEventInterval.setText(R.string.event_today);
                } else {
                    textViewEventInterval.setText(DateTimeUtil.getDateDiff(event.getStart_date())
                            + " " + this.getString(R.string.day_suffix));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }
}
