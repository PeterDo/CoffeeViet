package com.coffeeteam.coffeeviet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.Comment;
import com.coffeeteam.coffeeviet.entities.Event;
import com.coffeeteam.coffeeviet.entities.MenuItem;
import com.coffeeteam.coffeeviet.views.adapters.EventListCardViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 4/2/2017.
 */

public class EventListFragment extends Fragment {
    private static final String TAG = EventListFragment.class.getSimpleName();

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerViewUserComments; // primary RecyclerView
    private RecyclerView.Adapter userCommentSAdapter; // custom adapter for RecyclerView
    private String firebaseKey;

    private DatabaseReference databaseReference;
    private Query query;

    public EventListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        // set up xml layout for this fragment
        final ViewGroup viewGroup = (ViewGroup) inflater.inflate(
                R.layout.coffee_shop_detail_event_list_fragment, container, false);
        // create sample data. Just for Testing
        Bundle bundle = this.getArguments();
        this.firebaseKey = bundle.getString("coffeeShop_key");
        Log.d(TAG, "Firebase key : " + firebaseKey);
        this.databaseReference = FirebaseDatabase.getInstance().getReference();
        this.query = this.databaseReference.child("shop-events").child(this.firebaseKey);
        this.query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Event> listOfEvents = new ArrayList<>();
                for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                    //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                    Event item = shopDataSnapshot.getValue(Event.class);
                    //   item.setIsFood(true); // this is food, not drink
                    listOfEvents.add(item);
                    Log.d(TAG, "Menu item information : " + item.toString());
                    // Toast.makeText(getApplication(), "shop: " + shop.getName(), Toast.LENGTH_SHORT).show();
                }
                prepareViewContent(viewGroup, listOfEvents);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * @param viewGroup
     * @param listOfEvents
     */
    private void prepareViewContent(ViewGroup viewGroup, List<Event> listOfEvents) {
        this.userCommentSAdapter = new EventListCardViewAdapter(this.getContext(), listOfEvents);

        this.recyclerViewUserComments = (RecyclerView) viewGroup.findViewById(R.id.events_recycler_view);
        this.linearLayoutManager = new LinearLayoutManager(this.getActivity());
        this.recyclerViewUserComments.setLayoutManager(this.linearLayoutManager);
        this.recyclerViewUserComments.setHasFixedSize(true);
        this.recyclerViewUserComments.setAdapter(this.userCommentSAdapter);
        this.recyclerViewUserComments.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                AHBottomNavigation ahBottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.coffee_shop_detail_bottom_navigation);
                if (dy > 0) {

                    ahBottomNavigation.setVisibility(View.INVISIBLE);
                } else if (dy < 0) {
                    ahBottomNavigation.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
