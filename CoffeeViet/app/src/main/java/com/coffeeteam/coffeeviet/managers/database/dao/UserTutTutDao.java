package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.IUserTutDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.IUserTutSchema;
import com.coffeeteam.coffeeviet.entities.UserTut;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Hau-Do on 15/03/2017.
 */

/**
 * USER Data Access Object class.
 */
public class UserTutTutDao extends DbContentProvider implements IUserTutSchema, IUserTutDao {

    private Cursor mCursor;
    private List<UserTut> mUserList;
    private ContentValues mInitialValues;

    public UserTutTutDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public UserTut fetchUserById(int userId) {
        final String selectionArgs[] = {String.valueOf(userId)};
        final String selection = USER_ID + " = ?";
        UserTut user = new UserTut();
        mCursor = super.query(USER_TABLE, USER_COLUMNS, selection,
                selectionArgs, USER_NAME);
        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                user = cursorToEntity(mCursor);
                mCursor.moveToNext();
            }
            mCursor.close();
        }

        return user;
    }

    @Override
    public List<UserTut> fetchAllUsers() {
        mUserList = new ArrayList<UserTut>();
        mCursor = super.query(USER_TABLE, USER_COLUMNS, null,
                null, ID);

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                UserTut user = cursorToEntity(mCursor);
                mUserList.add(user);
                mCursor.moveToNext();
            }
            mCursor.close();
        }

        return mUserList;
    }


    public boolean addUser(UserTut user) {

        setContentValue(user);
        try {
            return super.insert(USER_TABLE, getContentValue()) > 0;
        } catch (SQLiteConstraintException ex) {
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    public boolean addUser(List<UserTut> userTuts) {
        try {
            mDb.beginTransaction();
            for (UserTut userTut : userTuts) {
                addUser(userTut);
            }
            mDb.setTransactionSuccessful();
        } finally {
            mDb.endTransaction();
        }
        return true;
    }

    protected UserTut cursorToEntity(Cursor cursor) {

        UserTut user = new UserTut();

        int idIndex;
        int userIdIndex;
        int userNameIndex;
        int emailIndex;
        int dateIndex;

        if (cursor != null) {
            if (cursor.getColumnIndex(ID) != -1) {
                idIndex = cursor.getColumnIndexOrThrow(ID);
                user.setId(cursor.getInt(idIndex));
            }
            if (cursor.getColumnIndex(USER_ID) != -1) {
                userIdIndex = cursor.getColumnIndexOrThrow(USER_ID);
                user.setUserId(cursor.getInt(userIdIndex));
            }
            if (cursor.getColumnIndex(USER_NAME) != -1) {
                userNameIndex = cursor.getColumnIndexOrThrow(USER_NAME);
                user.setUsername(cursor.getString(userNameIndex));
            }
            if (cursor.getColumnIndex(USER_EMAIL) != -1) {
                emailIndex = cursor.getColumnIndexOrThrow(USER_EMAIL);
                user.setEmail(cursor.getString(emailIndex));
            }
            if (cursor.getColumnIndex(USER_DATE) != -1) {
                dateIndex = cursor.getColumnIndexOrThrow(USER_DATE);
                user.setCreatedDate(new Date(cursor.getLong(dateIndex)));
            }

        }
        return user;
    }

    private ContentValues getContentValue() {
        return mInitialValues;
    }

    private void setContentValue(UserTut user) {
        mInitialValues = new ContentValues();
        mInitialValues.put(USER_ID, user.getUserId());
        mInitialValues.put(USER_NAME, user.getUsername());
        mInitialValues.put(USER_EMAIL, user.getEmail());
        mInitialValues.put(USER_DATE, user.getCreatedDate().getTime());
    }


    @Override
    public boolean deleteAllUsers() {
        return super.delete(USER_TABLE, null, null) > 0;
    }
}
