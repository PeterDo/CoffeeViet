package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.coffeeteam.coffeeviet.entities.Location;
import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.ILocationDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.ILocationSchema;

import java.util.List;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public class LocationDao extends DbContentProvider implements ILocationSchema, ILocationDao {

    private Cursor mCursor;
    private List<Location> mLocationList;
    private ContentValues mInitialValues;

    public LocationDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    protected <T> T cursorToEntity(Cursor cursor) {
        return null;
    }
}
