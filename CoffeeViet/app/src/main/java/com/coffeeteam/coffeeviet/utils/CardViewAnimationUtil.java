package com.coffeeteam.coffeeviet.utils;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.view.animation.DecelerateInterpolator;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.views.view_interfaces.ViewHolderFieldAccess;

/**
 * Created by TNS on 4/10/2017.
 * All animations related to {@link CardView}.
 * Implementation of all essential animations for cardview following Google material design.
 */

public class CardViewAnimationUtil {

    private CardViewAnimationUtil() {
    }

    /**
     * Make 'lift on touch' animation when user touch the specified cardview as parameter
     *
     * @param context               : {@link Context} to user loading {@link android.view.animation.Animation}
     * @param viewHolderFieldAccess : Implementation of {@link ViewHolderFieldAccess} to be set animation.
     *                              <p>
     *                              Usually, it's often custom of {@link android.support.v7.widget.RecyclerView.ViewHolder} class;
     */
    public static void flipOnTouchCardAnimation(Context context,
                                                ViewHolderFieldAccess viewHolderFieldAccess) {
        CardView cardView = viewHolderFieldAccess.getCardView();
        // make sure card view is valid CardView
        if (cardView != null) {
            // check if current device is Android 5 (supported material design)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                StateListAnimator stateListAnimator;
                stateListAnimator = AnimatorInflater
                        .loadStateListAnimator(context, R.animator.lift_on_touch);
                cardView.setStateListAnimator(stateListAnimator);
            }
        }
    }

    /**
     * Run Cardview appearance animation
     *
     * @param context               : {@link Context} to user loading {@link android.view.animation.Animation} and processing {@link Resources}
     * @param viewHolderFieldAccess : Implementation of {@link ViewHolderFieldAccess} to be set animation.
     *                              <p>
     *                              Usually, it's often custom of {@link android.support.v7.widget.RecyclerView.ViewHolder} class;
     */
    public static void animateCardViewAppearance(Context context, ViewHolderFieldAccess viewHolderFieldAccess, int position) {

        // make sure position is valid to start animation
        if (viewHolderFieldAccess.getShouldAnimateValue()) {
            // get CardView object
            CardView cardView = viewHolderFieldAccess.getCardView();
            if (cardView != null) {
                // update lastAnimation value
                int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
                cardView.setTranslationY(screenHeight);
                cardView.animate()
                        .translationY(0)
                        .setInterpolator(new DecelerateInterpolator(3.f))
                        .setDuration(700 + position * 10)
                        .start();
                // update shouldAnimation value of ViewHolder
                viewHolderFieldAccess.shouldAnimate(false);
            }
        }
    }


}
