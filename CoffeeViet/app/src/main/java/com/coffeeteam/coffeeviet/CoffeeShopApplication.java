package com.coffeeteam.coffeeviet;

import android.app.Application;
import android.content.res.Resources;

/**
 * Created by TNS on 4/11/2017.
 */

public class CoffeeShopApplication extends Application {

    protected static CoffeeShopApplication coffeeShopApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        coffeeShopApplication = this;
    }

    public static Resources getReSources() {
        return coffeeShopApplication.getResources();
    }
}
