package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 21/03/2017.
 */

public interface ICommentSchema {
    public static final String COMMENT_TABLE = "COMMENT";

    public static final String ID = "_id";
    public static final String COMMENT_ID = "id";
    public static final String COMMENT_TIME = "time";
    public static final String COMMENT_TEXT  = "text";
    public static final String COMMENT_URL_IMAGE = "url_image";
    public static final String COMMENT_USER_ID = "user_id";
    public static final String COMMENT_EVENT_ID = "event_id";

    public static final String COMMENT_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + COMMENT_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + COMMENT_ID
            + " TEXT, "
            + COMMENT_TIME
            + " TEXT, "
            + COMMENT_TEXT
            + " TEXT,"
            + COMMENT_URL_IMAGE
            + " TEXT, "
            + COMMENT_USER_ID
            + " TEXT, "
            + COMMENT_EVENT_ID
            + " TEXT"
            + ")";

    public static final String[] COMMENT_COLUMNS = new String[]{ID,
            COMMENT_ID, COMMENT_TIME, COMMENT_TEXT, COMMENT_URL_IMAGE, COMMENT_USER_ID, COMMENT_EVENT_ID};
}
