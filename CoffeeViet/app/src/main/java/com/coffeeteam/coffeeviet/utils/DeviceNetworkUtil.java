package com.coffeeteam.coffeeviet.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by TNS on 5/9/2017.
 */

public class DeviceNetworkUtil {

    private static final String TAG = DeviceNetworkUtil.class.getSimpleName();

    public static final String DEVICE_NETWORK_STATUS = "device_network_status";
    public static final String STATUS_AVAILABLE = "status_available";
    public static final String STATUS_UNAVAILABLE = "status_unavailable";

    /**
     * Private constructor for non-instantiability
     */
    private DeviceNetworkUtil() {

    }

    /**
     * Check device network connection status
     *
     * @return : true if status is available otherwise, returns false
     */
    public static boolean isNetworkAvailable(Context context) {
        boolean status = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            // get NetworkInfor
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                networkInfo = connectivityManager.getNetworkInfo(1);
                if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage() + ex.getStackTrace().toString(), ex);
            status = false;
        }
        return status;
    }
}
