package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public interface IFriendRelationshipSchema {
    public static final String FRIEND_RELATIONSHIP_TABLE = "FRIEND_RELATIONSHIP";

    public static final String ID = "_id";
    public static final String FRIEND_RELATIONSHIP_USER_ID = "user_id";
    public static final String FRIEND_RELATIONSHIP_FRIEND_ID = "friend_id";
    public static final String FRIEND_RELATIONSHIP_TIME = "relationship_time";

    public static final String FRIEND_RELATIONSHIP_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + FRIEND_RELATIONSHIP_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + FRIEND_RELATIONSHIP_USER_ID
            + " TEXT NOT NULL, "
            + FRIEND_RELATIONSHIP_FRIEND_ID
            + " TEXT NOT NULL, "
            + FRIEND_RELATIONSHIP_TIME
            + " TEXT"
            + ")";

    public static final String[] FRIEND_RELATIONSHIP_COLUMNS = new String[]{ID,
            FRIEND_RELATIONSHIP_USER_ID, FRIEND_RELATIONSHIP_FRIEND_ID, FRIEND_RELATIONSHIP_TIME};

}
