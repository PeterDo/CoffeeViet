package com.coffeeteam.coffeeviet.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;

import com.coffeeteam.coffeeviet.R;

/**
 * Created by TNS on 4/11/2017.
 * All custom {@link android.view.animation.Animation} for view components
 */
public class CustomAnimationUtil {

    // public static String FAVORITE_TAG = Resources.getSystem().getString(R.string.favorited_tag);
    // public static String UNFAVORITE_TAG = Resources.getSystem().getString(R.string.unfavorite_tag);

    // Animation constants
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);


    public static void favoriteIconAnimate(final ImageButton favoriteImageButton) {
        // define set of animations
        AnimatorSet setOfAnimators = new AnimatorSet();


        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(favoriteImageButton, "scaleX", 0.2f, 1f);
        bounceAnimX.setDuration(300);
        bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(favoriteImageButton, "scaleY", 0.2f, 1f);
        bounceAnimY.setDuration(300);
        bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
        bounceAnimY.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                String tag = (String) favoriteImageButton.getTag();
                //   if (tag.equals(UNFAVORITE_TAG)) {
                favoriteImageButton.setImageResource(R.drawable.ic_favorite_primary_color_24dp);
                //  } else if (tag.equals(FAVORITE_TAG)) {
                //      favoriteImageButton.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                //   }
            }
        });

        // set play definition
        setOfAnimators.play(bounceAnimX).with(bounceAnimY);

        // start animation
        setOfAnimators.start();
    }


    /**
     * Show snack bar with specified message
     *
     * @param viewContainer
     * @param message
     * @param textColorHex
     */
    public static void showMessageWithSnackBar(View viewContainer,
                                               String message,
                                               String textColorHex,
                                               String buttonLabel) {
        Snackbar snackbar = Snackbar
                .make(viewContainer, message, Snackbar.LENGTH_LONG);
        if (!buttonLabel.isEmpty() && buttonLabel != null) {
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // doing nothing
                }
            });
        }
        // set custom style for Snackbar if available
        if (textColorHex != null && !textColorHex.isEmpty()) {
            snackbar.setActionTextColor(Color.parseColor(textColorHex));
        }
        snackbar.show();
    }

    /**
     * @param context
     * @param view
     */
    public static void startFadeinAnimation(Context context, View view, int duration) {
        Animation startAnimation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        startAnimation.setDuration(duration);
        view.startAnimation(startAnimation);
    }
}
