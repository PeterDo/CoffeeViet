package com.coffeeteam.coffeeviet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.MenuItem;
import com.coffeeteam.coffeeviet.views.adapters.MenuDrinkItemListViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 5/13/2017.
 */

public class MenuDrinkItemListFragment extends Fragment {

    private static final String TAG = MenuDrinkItemListFragment.class.getSimpleName();

    private ListView listViewMenuDrinkItems;
    private MenuDrinkItemListViewAdapter menuItemListViewAdapter;
    private DatabaseReference databaseReference;
    private Query query;
    private String firebaseKey;


    public MenuDrinkItemListFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container
            , Bundle savedInstanceState) {
        // set up xml layout for this fragment
        final ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.coffee_shop_menu_fragment, container, false);
        // get bundle argument from parent Activity
        Bundle bundle = this.getArguments();
        this.firebaseKey = bundle.getString("coffeeShop_key");
        Log.d(TAG, "Firebase key : " + firebaseKey);
        // create sample data
        final List<MenuItem> menuItems = new ArrayList<>();
        // start to fetch firebase data
        if (!this.firebaseKey.isEmpty()) {
            // get shop foods
            this.databaseReference = FirebaseDatabase.getInstance().getReference();
            this.query = this.databaseReference.child("shop-drinks").child(this.firebaseKey);
            this.query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                        //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                        MenuItem item = shopDataSnapshot.getValue(MenuItem.class);
                        item.setIsFood(true); // this is food, not drink
                        menuItems.add(item);
                        Log.d(TAG, "Menu item information : " + item.toString());
                        // Toast.makeText(getApplication(), "shop: " + shop.getName(), Toast.LENGTH_SHORT).show();
                    }
                    prepareContent(viewGroup, menuItems);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Utility method for preparing view content.
     * This method will be used inside addValueEventListener method of {@link Query} object
     *
     * @param viewGroup       : {@link ViewGroup} from onCreateView method
     * @param listOfMenuItems : List of {@link MenuItem} will be displayed
     */
    private void prepareContent(ViewGroup viewGroup, List<MenuItem> listOfMenuItems) {
        this.listViewMenuDrinkItems = (ListView) viewGroup.findViewById(R.id.listview_menu_items);
        this.menuItemListViewAdapter = new MenuDrinkItemListViewAdapter(this.getActivity(),
                R.layout.coffee_shop_menu_fragment,
                listOfMenuItems);
        // make sure list view can be scrolled inside scroll nested view
        ViewCompat.setNestedScrollingEnabled(this.listViewMenuDrinkItems, true);
        // set adapter for list view
        this.listViewMenuDrinkItems.setAdapter(this.menuItemListViewAdapter);
        this.listViewMenuDrinkItems.setEmptyView(viewGroup.findViewById(R.id.textview_emptylist_placeholder));
        this.listViewMenuDrinkItems.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                AHBottomNavigation ahBottomNavigation = (AHBottomNavigation) getActivity().
                        findViewById(R.id.coffee_shop_detail_bottom_navigation);
                // scroll down
                if (lastFirstVisibleItem < i) {
                    ahBottomNavigation.setVisibility(View.INVISIBLE);
                } else if (lastFirstVisibleItem > i) { // scroll up
                    ahBottomNavigation.setVisibility(View.VISIBLE);
                }
                lastFirstVisibleItem = i;
            }
        });
    }
}
