package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public interface ILocationSchema {
    public static final String LOCATION_TABLE = "LOCATION";

    public static final String ID = "_id";
    public static final String LOCATION_LATITUDE= "latitude";
    public static final String LOCATION_LONGITUDE = "longitude";

    public static final String LOCATION_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + LOCATION_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + LOCATION_LATITUDE
            + " FLOAT, "
            + LOCATION_LONGITUDE
            + " FLOAT"
            + ")";

    public static final String[] LOCATION_COLUMNS = new String[]{ID,
            LOCATION_LATITUDE, LOCATION_LONGITUDE};
}
