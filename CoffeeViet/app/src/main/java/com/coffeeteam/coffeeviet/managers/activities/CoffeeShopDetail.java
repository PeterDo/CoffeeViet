package com.coffeeteam.coffeeviet.managers.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.fragments.EventListFragment;
import com.coffeeteam.coffeeviet.fragments.MenuDrinkItemListFragment;
import com.coffeeteam.coffeeviet.fragments.MenuFoodItemListFragment;
import com.coffeeteam.coffeeviet.fragments.UserCommentFragment;
import com.coffeeteam.coffeeviet.utils.CustomAnimationUtil;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;
import com.coffeeteam.coffeeviet.utils.StringFormatUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CoffeeShopDetail extends AppCompatActivity {
    // TAG
    public static final String TAG = CoffeeShopDetail.class.getSimpleName();

    // Intent parameter keys
    public static final String DETAIL_COFFEE_SHOP_LATITUDE = "detail_coffee_shop_latitude";
    public static final String DETAIL_COFFEE_SHOP_LONGITUDE = "detail_coffee_shop_longitude";
    public static final String DETAIL_COFFEE_SHOP_NAME = "detail_coffee_shop_name";

    private AHBottomNavigation bottomNavigationBar;
    private ViewPager viewPager;

    private TextView listHeaderTextView;

    // primary fragments
    private MenuFoodItemListFragment menuFoodItemListFragment;
    private MenuDrinkItemListFragment menuDrinkItemListFragment;
    private UserCommentFragment userCommentFragment;
    private EventListFragment eventListFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }
        setContentView(R.layout.activity_coffee_shop_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        // inside your activity (if you did not enable transitions in your theme)
        // set an exit transition
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            this.getWindow().setEnterTransition(new Explode());
        }
        setSupportActionBar(toolbar);
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        this.listHeaderTextView = (TextView) this.findViewById(R.id.coffee_detail_list_text_header);
        // initial fragments
        this.eventListFragment = new EventListFragment();
        this.menuFoodItemListFragment = new MenuFoodItemListFragment();
        this.userCommentFragment = new UserCommentFragment();
        this.menuDrinkItemListFragment = new MenuDrinkItemListFragment();
        /**
         * get Data
         */
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        CoffeeShop coffeeShop = bundle.getParcelable(PrefsUtil.CURRENT_SHOP);
        Log.d(TAG, coffeeShop.toString());
        // get argument for fragments
        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putString("coffeeShop_key", coffeeShop.getFirebaseKey());
        // add arguments for 2 menu item fragments
        this.menuFoodItemListFragment.setArguments(fragmentBundle);
        this.menuDrinkItemListFragment.setArguments(fragmentBundle);
        this.userCommentFragment.setArguments(fragmentBundle);
        this.eventListFragment.setArguments(fragmentBundle);
        // make sure we have valid bundle argument
        this.prepareCoffeeDetailInformationView(coffeeShop);


    }


    /**
     * In this screen we use custom bottom navigation bar from https://github.com/aurelhubert/ahbottomnavigation
     * to make a swiping fragments for use to see essential information of current selected coffee shop.
     * NOTE : Make sure {@link AHBottomNavigation} of this class is valid
     * before invoking this method.
     */
    private void prepareBottomNavigationBar() {
        if (this.bottomNavigationBar != null) {
            // Create items
            AHBottomNavigationItem foodItemMenu = new AHBottomNavigationItem("Món ăn", R.drawable.icon_food, R.color.colorPrimary);
            AHBottomNavigationItem drinkItemMenu = new AHBottomNavigationItem("Nước uống", R.drawable.ic_local_bar_black_24dp, R.color.colorPrimary);
            AHBottomNavigationItem itemComments = new AHBottomNavigationItem("Bình luận", R.drawable.ic_message_black_24dp, R.color.colorPrimary);
            AHBottomNavigationItem itemEvents = new AHBottomNavigationItem("Sự kiện", R.drawable.ic_event_black_24dp, R.color.colorPrimary);
            // Add items
            this.bottomNavigationBar.addItem(foodItemMenu);
            this.bottomNavigationBar.addItem(drinkItemMenu);
            this.bottomNavigationBar.addItem(itemComments);
            this.bottomNavigationBar.addItem(itemEvents);
            // Disable the translation inside the CoordinatorLayout
            this.bottomNavigationBar.setBehaviorTranslationEnabled(true);
            this.bottomNavigationBar.setTranslucentNavigationEnabled(true);
            // Change colors
            this.bottomNavigationBar.setAccentColor(Color.parseColor("#C62828"));
            this.bottomNavigationBar.setInactiveColor(Color.parseColor("#747474"));
            // Manage titles
            this.bottomNavigationBar.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
            this.bottomNavigationBar.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
            this.bottomNavigationBar.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
            // Set current item programmatically,default is 0
            this.bottomNavigationBar.setCurrentItem(0);
            // register click listener
            this.bottomNavigationBar.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
                @Override
                public boolean onTabSelected(int position, boolean wasSelected) {
                    viewPager.setCurrentItem(position);
                    // change header text
                    if (listHeaderTextView != null) {
                        if (position == 0) {
                            // food
                            listHeaderTextView.setText(R.string.food_header);
                        } else if (position == 1) {
                            // drink
                            listHeaderTextView.setText(R.string.drink_header);
                        } else if (position == 2) {
                            // comment
                            listHeaderTextView.setText(R.string.comments_label);
                        } else if (position == 3) {
                            // event
                            listHeaderTextView.setText(R.string.events_label);
                        }
                        CustomAnimationUtil.startFadeinAnimation(getApplicationContext(), listHeaderTextView, 1500);
                    }
                    return true;
                }
            });

        }
    }


    /**
     * Prepare and show detail of specified coffee shop
     *
     * @param coffeeShop : {@link CoffeeShop} to be displayed detail
     */
    private void prepareCoffeeDetailInformationView(final CoffeeShop coffeeShop) {

        TextView coffeeShopNameTextView = (TextView) this.findViewById(R.id.textview_detail_coffee_shop_name);
        coffeeShopNameTextView.setText(coffeeShop.getName());
        TextView coffeeShopWorkingTimeTextView = (TextView) this.findViewById(R.id.textview_detail_working_time);
        coffeeShopWorkingTimeTextView.setText(this.getString(R.string.open_at) + coffeeShop.getOpening_time()
                + " - " + this.getString(R.string.close_at) + coffeeShop.getClosing_time());
        // detect working time
        try {
            TextView workingStatusTextView = (TextView) this.findViewById(R.id.textview_detail_workk_status);
            if (StringFormatUtil.isOpeningTime(coffeeShop.getOpening_time(), coffeeShop.getClosing_time())) {
                workingStatusTextView.setText(R.string.status_opening);
            } else {
                workingStatusTextView.setText(R.string.status_closed);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        TextView coffeeShopAddressTextView = (TextView) this.findViewById(R.id.textview_detail_address);
        coffeeShopAddressTextView.setText(coffeeShop.getAddress());
        TextView coffeeShopLikeCountTextView = (TextView) this.findViewById(R.id.textview_detail_likes);
        coffeeShopLikeCountTextView.setText(coffeeShop.getLikeCount() + "");
        TextView coffeeShopShareCountTextView = (TextView) this.findViewById(R.id.textview_detail_shares);
        coffeeShopShareCountTextView.setText(coffeeShop.getShareCount() + "");
        final TextView coffeeCommentCountTextView = (TextView) this.findViewById(R.id.textview_detail_comments);
        coffeeCommentCountTextView.setText(coffeeShop.getCommentCount() + "");

        TextView textViewDistanceFromCurrentLocation = (TextView) this.findViewById(R.id.textview_detail_distance_from);

        // distance from current device's location
        String currentLatitude = PrefsUtil.getStringPrefValue(this.getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LATITUDE);
        String currentLongitude = PrefsUtil.getStringPrefValue(this.getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LONGITUDE);
        if (!currentLatitude.equals(PrefsUtil.PREF_DEFAULT_VALUE)
                && !currentLongitude.equals(PrefsUtil.PREF_DEFAULT_VALUE)
                && coffeeShop.getLongitude() != null
                && coffeeShop.getLatitude() != null) {
            try {
                // start to calculate distance
                float[] results = new float[1];
                Location.distanceBetween(Double.parseDouble(currentLatitude),
                        Double.parseDouble(currentLongitude),
                        coffeeShop.getLatitude(),
                        coffeeShop.getLongitude(), results);
                // show information
                textViewDistanceFromCurrentLocation.setText(String.format("%.02f", results[0] / 1000f) + this.getString(R.string.distance_suffix))
                ;
            } catch (NumberFormatException e) {
                Log.d(TAG, "can not convert to String to Double", e);
                textViewDistanceFromCurrentLocation.setVisibility(View.INVISIBLE);
            }
        }

        // setFillViewport to true for better look
        NestedScrollView nestedScrollView = (NestedScrollView) this.findViewById(
                R.id.coffee_detail_nestedscrollview);
        nestedScrollView.setFillViewport(true);

        RatingBar ratingBar = (RatingBar) this.findViewById(R.id.rating_bar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        // change background tin of fab
        fab.setBackgroundTintList(this.getResources().getColorStateList(R.color.second_fab_background_tin));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // move to Map activity and direct use from current location to selected coffee shop
                Intent coffeeMapIntent = new Intent(getApplicationContext(), NearbyCoffeeShopsActivity.class);
                // indicate action type
                coffeeMapIntent.putExtra(NearbyCoffeeShopsActivity.MAP_ACTION_TYPE, // key
                        NearbyCoffeeShopsActivity.ACTION_FIREBASE_COFFEE_SHOP_DETAIL_DIRECTION // tell the map to direct user to selected detail coffee shop.
                );
                // put intent param
                coffeeMapIntent.putExtra(DETAIL_COFFEE_SHOP_LATITUDE, coffeeShop.getLatitude().toString());
                coffeeMapIntent.putExtra(DETAIL_COFFEE_SHOP_LONGITUDE, coffeeShop.getLongitude().toString());
                coffeeMapIntent.putExtra(DETAIL_COFFEE_SHOP_NAME, coffeeShop.getName());
                // start activity
                startActivity(coffeeMapIntent);
            }
        });


        final ImageView headerImage = (ImageView) this.findViewById(R.id.imageview_header);
        if (coffeeShop.getPhotoUrl() != null
                && !coffeeShop.getPhotoUrl().isEmpty()) {
            // start to load image
            Picasso.with(this.getApplicationContext())
                    .load(coffeeShop.getPhotoUrl())
                    .fit().centerCrop()
                    .into(headerImage);
        } else {
            Picasso.with(this.getApplicationContext())
                    .load(R.drawable.detail_coffeeshop_placeholder)
                    .fit()
                    .centerCrop()
                    .into(headerImage);
        }
        // view pager
        this.viewPager = (ViewPager) this.findViewById(R.id.coffee_detail_viewpager);
        this.setUpCoffeeShopDetailViewPagers();
        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (bottomNavigationBar != null) {
                    bottomNavigationBar.setCurrentItem(position);
                }
                // change header text
                if (listHeaderTextView != null) {
                    if (position == 0) {
                        // food
                        listHeaderTextView.setText(R.string.food_header);
                    } else if (position == 1) {
                        // drink
                        listHeaderTextView.setText(R.string.drink_header);
                    } else if (position == 2) {
                        // comment
                        listHeaderTextView.setText(R.string.comments_label);
                    } else if (position == 3) {
                        // event
                        listHeaderTextView.setText(R.string.events_label);
                    }
                    CustomAnimationUtil.startFadeinAnimation(getApplicationContext(), listHeaderTextView, 1500);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Bottom navigation bar
        this.bottomNavigationBar = (AHBottomNavigation)
                this.findViewById(R.id.coffee_shop_detail_bottom_navigation);
        this.prepareBottomNavigationBar();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.coffee_shop_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Adapter for view pager to swap inner fragments
     */
    private class CoffeeShopDetailViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> listOfFragments = new ArrayList<>();
        private final List<String> listOfTitles = new ArrayList<>();

        public CoffeeShopDetailViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        /**
         * Get {@link android.app.Fragment} by index
         *
         * @param position : index of element in list
         * @return
         */
        @Override
        public Fragment getItem(int position) {
            return this.listOfFragments.get(position);
        }

        @Override
        public int getCount() {
            return this.listOfFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.listOfTitles.get(position);
        }

        /**
         * Add new Fragment Item into list of fragments.
         *
         * @param fragment : {@link android.app.Fragment}
         * @param title
         */
        public void addFragment(Fragment fragment, String title) {
            this.listOfFragments.add(fragment);
            this.listOfTitles.add(title);
        }
    }

    private void setUpCoffeeShopDetailViewPagers() {
        CoffeeShopDetailViewPagerAdapter viewPagerAdapter = new CoffeeShopDetailViewPagerAdapter(
                this.getSupportFragmentManager());
        // start to add fragments into view pagers
        if (this.menuFoodItemListFragment != null) {
            viewPagerAdapter.addFragment(this.menuFoodItemListFragment,
                    this.getString(R.string.coffee_shops_tab_title)); // menu items fragment
        }
        if (this.menuDrinkItemListFragment != null) {
            viewPagerAdapter.addFragment(this.menuDrinkItemListFragment,
                    this.getString(R.string.coffee_shops_tab_title)); // menu items fragment
        }
        if (this.userCommentFragment != null) {
            viewPagerAdapter.addFragment(this.userCommentFragment,
                    this.getString(R.string.favorite_shops_tab_title)); // user comments fragment
        }
        if (this.eventListFragment != null)
            viewPagerAdapter.addFragment(this.eventListFragment,
                    this.getString(R.string.friends_tab_title)); // event list fragment
        // set up adapter for view pager
        if (this.viewPager != null) {
            this.viewPager.setAdapter(viewPagerAdapter);
        }


    }


    @Override
    protected void onStop() {
        super.onStop();
    }


}
