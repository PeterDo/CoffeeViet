package com.coffeeteam.coffeeviet.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class CoffeeShop implements Parcelable {

    private String creator_id;
    private String address;
    private String closing_time;
    private int commentCount = 0;
    public Map<String, Boolean> comments = new HashMap<>();
    private String creating_time;
    private int likeCount = 0;
    public Map<String, Boolean> likes = new HashMap<>();
    private String name;
    private String opening_time;
    private int shareCount = 0;
    public Map<String, Boolean> shares = new HashMap<>();
    private String photoUrl;
    private String description;
    private Double latitude;
    private Double longitude;
    private String firebaseKey;


    /**
     * Constructor
     */
    public CoffeeShop() {

    }

    public CoffeeShop(String creator_id, String name, String address, String opening_time, String closing_time) {
        this.creator_id = creator_id;
        this.name = name;
        this.address = address;
        this.opening_time = opening_time;
        this.closing_time = closing_time;
    }

    protected CoffeeShop(Parcel in) {
        creator_id = in.readString();
        address = in.readString();
        closing_time = in.readString();
        commentCount = in.readInt();
        creating_time = in.readString();
        likeCount = in.readInt();
        name = in.readString();
        opening_time = in.readString();
        shareCount = in.readInt();
        photoUrl = in.readString();
        description = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        firebaseKey = in.readString();
    }

    public static final Creator<CoffeeShop> CREATOR = new Creator<CoffeeShop>() {
        @Override
        public CoffeeShop createFromParcel(Parcel in) {
            return new CoffeeShop(in);
        }

        @Override
        public CoffeeShop[] newArray(int size) {
            return new CoffeeShop[size];
        }
    };

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getAddress() {
        return address;
    }


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public Map<String, Boolean> getComments() {
        return comments;
    }

    public void setComments(Map<String, Boolean> comments) {
        this.comments = comments;
    }

    public String getCreating_time() {
        return creating_time;
    }

    public void setCreating_time(String creating_time) {
        this.creating_time = creating_time;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public Map<String, Boolean> getLikes() {
        return likes;
    }

    public void setLikes(Map<String, Boolean> likes) {
        this.likes = likes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public Map<String, Boolean> getShares() {
        return shares;
    }

    public void setShares(Map<String, Boolean> shares) {
        this.shares = shares;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Return pair string pattern of latitude and longitude with
     * character ',' is separator
     *
     * @return
     */
    public String getLocationPairString() {
        return this.latitude.toString() + "," + this.longitude.toString();
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("creator_id", creator_id);
        result.put("address", address);
        result.put("closing_time", closing_time);
        result.put("commentCount", commentCount);
        result.put("comments", comments);
        result.put("creating_time", creating_time);
        result.put("likeCount", likeCount);
        result.put("likes", likes);
        result.put("name", name);
        result.put("opening_time", opening_time);
        result.put("shareCount", shareCount);
        result.put("shares", shares);
        result.put("description", this.description);
        result.put("latitude", this.latitude);
        result.put("longitude", this.longitude);
        result.put("firebaseKey", this.firebaseKey);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(creator_id);
        parcel.writeString(address);
        parcel.writeString(closing_time);
        parcel.writeInt(commentCount);
        parcel.writeString(creating_time);
        parcel.writeInt(likeCount);
        parcel.writeString(name);
        parcel.writeString(opening_time);
        parcel.writeInt(shareCount);
        parcel.writeString(photoUrl);
        parcel.writeString(description);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(firebaseKey);
    }


    @Override
    public String toString() {
        return "CoffeeShop{" +
                "creator_id='" + creator_id + '\'' +
                ", address='" + address + '\'' +
                ", closing_time='" + closing_time + '\'' +
                ", commentCount=" + commentCount +
                ", comments=" + comments +
                ", creating_time='" + creating_time + '\'' +
                ", likeCount=" + likeCount +
                ", likes=" + likes +
                ", name='" + name + '\'' +
                ", opening_time='" + opening_time + '\'' +
                ", shareCount=" + shareCount +
                ", shares=" + shares +
                ", photoUrl='" + photoUrl + '\'' +
                ", description='" + description + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", firebaseKey='" + firebaseKey + '\'' +
                '}';
    }

    public String getFirebaseKey() {
        return firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }
}
