package com.coffeeteam.coffeeviet.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.User;
import com.coffeeteam.coffeeviet.utils.DeviceNetworkUtil;
import com.coffeeteam.coffeeviet.views.adapters.FriendListCardAdapter;
import com.coffeeteam.coffeeviet.views.view_interfaces.OnDataRefreshListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 3/13/2017.
 */

public class FriendListFragment extends Fragment implements OnDataRefreshListener {

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerViewFriendList;
    private RecyclerView.Adapter friendListAdapter;
    private LinearLayout offlineView;
    private FloatingActionButton fab;
    private SwipeRefreshLayout swipeRefreshLayout;

    private DatabaseReference databaseReference;
    private Query query;


    public FriendListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        // set up xml layout for this fragment
        final ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.friend_list_fragment, container, false);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab); // get fab from Main Activity
        this.swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.primary_swipeRefreshLayout);
        this.recyclerViewFriendList = (RecyclerView) viewGroup.findViewById(R.id.friend_list_recycler_view);
        this.offlineView = (LinearLayout) viewGroup.findViewById(R.id.offline_inform_view);
        //
        this.refreshFirebaseData();
        return viewGroup;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Prepare view content
     *
     * @param listOfFriends : data for adapter
     */
    private void prepareViewContent(List<User> listOfFriends) {
        friendListAdapter = new FriendListCardAdapter(getActivity(), listOfFriends);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewFriendList.setLayoutManager(linearLayoutManager);
        recyclerViewFriendList.setHasFixedSize(true);
        recyclerViewFriendList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // super.onScrolled(recyclerView, dx, dy);
                // hide fab when user scroll down the recycler view
                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });

        recyclerViewFriendList.setAdapter(friendListAdapter);


    }

    /**
     *
     */
    @Override
    public void refreshFirebaseData() {
        if (DeviceNetworkUtil.isNetworkAvailable(this.getContext())) {
            this.recyclerViewFriendList.setVisibility(View.VISIBLE);
            this.offlineView.setVisibility(View.GONE);
            this.databaseReference = FirebaseDatabase.getInstance().getReference();
            this.query = this.databaseReference.child("users");
            this.query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    List<User> listOfFriends = new ArrayList<>();
                    // start to get list of users
                    for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                        //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                        User user = shopDataSnapshot.getValue(User.class);
                        listOfFriends.add(user);
                        // Toast.makeText(getApplication(), "shop: " + shop.getName(), Toast.LENGTH_SHORT).show();
                    }
                    prepareViewContent(listOfFriends);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            // set freshing to fasle
            if (this.swipeRefreshLayout.isRefreshing()) {
                this.swipeRefreshLayout.setRefreshing(false);
            }
        } else {
            this.doOfflineWork();
        }
    }

    @Override
    public void refreshLocalDatabase() {

    }

    @Override
    public void doOfflineWork() {
        if (this.recyclerViewFriendList != null && this.offlineView != null
                && this.fab != null && this.swipeRefreshLayout != null) {
            // network is available
            this.recyclerViewFriendList.setVisibility(View.GONE);
            this.offlineView.setVisibility(View.VISIBLE);
            fab.hide();
            // stop refreshing layout
            if (this.swipeRefreshLayout.isRefreshing()) {
                this.swipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}
