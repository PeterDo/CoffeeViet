package com.coffeeteam.coffeeviet.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.utils.DeviceNetworkUtil;
import com.coffeeteam.coffeeviet.views.adapters.CoffeeShopsCardViewAdapter;
import com.coffeeteam.coffeeviet.views.view_interfaces.OnDataRefreshListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by TNS on 3/13/2017.
 */

public class CoffeeShopsFragment extends Fragment implements OnDataRefreshListener {

    private static final String TAG = CoffeeShopsFragment.class.getSimpleName();
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerViewCoffeeShops;
    private RecyclerView.Adapter coffeeShopAdapter;
    private LinearLayout offlineView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton fab;
    private ViewGroup viewGroup; //


    private DatabaseReference databaseReference;
    private Query query;


    public CoffeeShopsFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // set up xml layout for this fragment
        this.viewGroup = (ViewGroup) inflater.inflate(R.layout.coffee_shops_fragment, container, false);

        this.recyclerViewCoffeeShops = (RecyclerView) viewGroup.findViewById(R.id.coffee_shop_recycler_view);
        this.offlineView = (LinearLayout) viewGroup.findViewById(R.id.offline_inform_view);
        this.fab = (FloatingActionButton) getActivity().findViewById(R.id.fab); // get fab from Main Activity
        this.swipeRefreshLayout = (SwipeRefreshLayout)
                this.getActivity().findViewById(R.id.primary_swipeRefreshLayout);
        //
        this.refreshFirebaseData();
        return this.viewGroup;
    }


    /**
     * @param listOfCoffeeShops
     */
    private void prepareViewContent(List<CoffeeShop> listOfCoffeeShops) {

        coffeeShopAdapter = new CoffeeShopsCardViewAdapter(getActivity(), listOfCoffeeShops);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewCoffeeShops.setLayoutManager(linearLayoutManager);
        recyclerViewCoffeeShops.setHasFixedSize(true);
        recyclerViewCoffeeShops.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // super.onScrolled(recyclerView, dx, dy);
                // hide fab when user scroll down the recycler view
                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });

        recyclerViewCoffeeShops.setAdapter(coffeeShopAdapter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * * Reload data from Firebase database
     */
    @Override
    public void refreshFirebaseData() {
        // make sure ViewGroup is valid
        if (this.viewGroup != null) {
            // make sure network state is available
            if (DeviceNetworkUtil.isNetworkAvailable(this.getContext())) {
                this.recyclerViewCoffeeShops.setVisibility(View.VISIBLE);
                this.offlineView.setVisibility(View.GONE);
                this.databaseReference = FirebaseDatabase.getInstance().getReference();
                this.query = this.databaseReference.child("shops");
                this.query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<CoffeeShop> listOfCoffeeShops = new ArrayList<>();
                        // start to get list of users
                        for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                            //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                            CoffeeShop coffeeShop = shopDataSnapshot.getValue(CoffeeShop.class);
                            coffeeShop.setFirebaseKey(shopDataSnapshot.getKey()); // get database Fire base
                            listOfCoffeeShops.add(coffeeShop);
                            Log.d(TAG, "CoffeeShop information : " + coffeeShop.toString());
                            // Toast.makeText(getApplication(), "shop: " + shop.getName(), Toast.LENGTH_SHORT).show();
                        }
                        prepareViewContent(listOfCoffeeShops);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {


                    }
                });
                // stop refreshing
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            } else {
                doOfflineWork();
            }
        }
    }

    @Override
    public void refreshLocalDatabase() {

    }

    @Override
    public void doOfflineWork() {
        // make sure all view components are valid
        if (this.recyclerViewCoffeeShops != null && this.offlineView != null && this.fab != null) {
            this.recyclerViewCoffeeShops.setVisibility(View.GONE);
            this.offlineView.setVisibility(View.VISIBLE);
            fab.hide();
        }
    }
}
