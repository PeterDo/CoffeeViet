package com.coffeeteam.coffeeviet.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.entities.Comment;
import com.coffeeteam.coffeeviet.entities.User;
import com.coffeeteam.coffeeviet.utils.DateTimeUtil;
import com.coffeeteam.coffeeviet.views.adapters.UserCommentCardViewAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;


/**
 * Created by TNS on 3/13/2017.
 */

/**
 * Overrided by Hau-Do on 5/23/2017
 */

public class UserCommentFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = UserCommentFragment.class.getSimpleName();

    private RecyclerView.Adapter mUserCommentAdapter; // custom adapter for RecyclerView
    private String mCoffeeShopKey;

    private DatabaseReference mShopReference;
    private DatabaseReference mCommentsReference;
    private ValueEventListener mShopListener;

    private EditText mCommentField;
    private Button mCommentButton;
    private RecyclerView mRecyclerViewUserComments;

    public UserCommentFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container
            , Bundle savedInstanceState) {

        // set up xml layout for this fragment
        final ViewGroup viewGroup = (ViewGroup) inflater.inflate(
                R.layout.coffee_shop_detail_user_comment_fragment, container, false);

        mCommentField = (EditText) viewGroup.findViewById(R.id.field_comment_text);
        mCommentButton = (Button) viewGroup.findViewById(R.id.button_shop_comment);
        mCommentButton.setOnClickListener(this);
        mRecyclerViewUserComments = (RecyclerView) viewGroup.findViewById(R.id.user_comments_recycler_view);
        mRecyclerViewUserComments.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        Bundle bundle = this.getArguments();
        mCoffeeShopKey = bundle.getString("coffeeShop_key");
        if (mCoffeeShopKey == null) {
            throw new IllegalArgumentException("mCoffeeShopKey = null");
        }
        mShopReference = FirebaseDatabase.getInstance().getReference()
                .child("shops").child(mCoffeeShopKey);
        mCommentsReference = FirebaseDatabase.getInstance().getReference().child("shop-comments").child(mCoffeeShopKey);
        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Add value event listener to the shop
        ValueEventListener shopListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get shop object and use the values to update the UI (handle later)
                CoffeeShop coffeeShop = dataSnapshot.getValue(CoffeeShop.class);
                /**
                 * ...
                 */
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to load the shop.",
                        Toast.LENGTH_SHORT).show();

            }
        };
        mShopReference.addValueEventListener(shopListener);

        // Keep copy of post listener so we can remove it when app stops
        mShopListener = shopListener;

        // Listen for comments
        mUserCommentAdapter = new UserCommentCardViewAdapter(this.getActivity(), mCommentsReference);
        mRecyclerViewUserComments.setAdapter(mUserCommentAdapter);
        mRecyclerViewUserComments.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                AHBottomNavigation ahBottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.coffee_shop_detail_bottom_navigation);
                if (dy > 0) {

                    ahBottomNavigation.setVisibility(View.INVISIBLE);
                } else if (dy < 0) {
                    ahBottomNavigation.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * Click event of mCommentButton
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_shop_comment) {
            shopComment();
        }
    }

    /**
     * Push user comment into Firebase database.
     */
    private void shopComment() {
        final String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference().child("users").child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        String username = user.getUsername();
                        // make sure user comment is no empty
                        String commentText = mCommentField.getText().toString();
                        // make sure comment's content is valid
                        if (!TextUtils.isEmpty(commentText)) {
                            Comment comment = new Comment(userId, username, commentText);
                            // set creation time of this comment
                            comment.setCreating_time(DateTimeUtil.toDateTimeFormatString(DateTimeUtil.DEFAULT_FORMAT_VALUE,
                                    new Date()));
                            mCommentsReference.push().setValue(comment);
                            mCommentField.setText(null);
                            // show toast to user the work status
                            Toast.makeText(getContext(), R.string.comment_pushed, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

}
