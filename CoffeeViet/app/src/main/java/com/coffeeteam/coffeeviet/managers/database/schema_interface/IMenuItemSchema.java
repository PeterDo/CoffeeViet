package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public interface IMenuItemSchema {
    public static final String MENU_ITEM_TABLE = "MENU_ITEM";

    public static final String ID = "_id";
    public static final String MENU_ITEM_ID = "id";
    public static final String MENU_ITEM_NAME = "name";
    public static final String MENU_ITEM_PRICE = "price";
    public static final String MENU_ITEM_COFFEE_SHOP_ID = "coffee_shop_id";
    public static final String MENU_ITEM_ACTIVE = "active";

    public static final String LOCATION_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + MENU_ITEM_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + MENU_ITEM_ID
            + " TEXT, "
            + MENU_ITEM_NAME
            + " TEXT NOT NULL"
            + MENU_ITEM_PRICE
            + " TEXT NOT NULL,"
            + MENU_ITEM_COFFEE_SHOP_ID
            + " TEXT, "
            + MENU_ITEM_ACTIVE
            + " BOOLEAN"
            + ")";

    public static final String[] MENU_ITEM_COLUMNS = new String[]{ID,
            MENU_ITEM_ID, MENU_ITEM_NAME, MENU_ITEM_PRICE, MENU_ITEM_COFFEE_SHOP_ID, MENU_ITEM_ACTIVE};
}
