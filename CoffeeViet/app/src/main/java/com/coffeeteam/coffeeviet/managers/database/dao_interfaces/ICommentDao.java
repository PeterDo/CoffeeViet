package com.coffeeteam.coffeeviet.managers.database.dao_interfaces;

import com.coffeeteam.coffeeviet.entities.Comment;

import java.util.List;

/**
 * Created by Hau-Do on 21/03/2017.
 */

public interface ICommentDao {
    public Comment fetchCommentById(int id);

    public List<Comment> fetchAllComments();

    public boolean addComment(Comment comment);

    public boolean addComment(List<Comment> commentList);

    public boolean deleteAllComments();
}
