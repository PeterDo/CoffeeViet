package com.coffeeteam.coffeeviet.views.adapters;

import android.content.Context;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;

import java.util.List;

/**
 * Created by TNS on 3/20/2017.
 */

public class FavoriteCoffeeShopListCardViewAdapter extends CoffeeShopsCardViewAdapter {

    public FavoriteCoffeeShopListCardViewAdapter(Context context, List<CoffeeShop> listOfCoffeeShops) {
        super(context, listOfCoffeeShops);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        // the only thing to do for this holder is to update icon for image button
        holder.imageButtonFavorite.setImageResource(R.drawable.ic_favorite_primary_color_24dp);
    }
}
