package com.coffeeteam.coffeeviet.managers.networks;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.GooglePlaceWebServiceGeometry;
import com.coffeeteam.coffeeviet.utils.JsonParserUtil;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 5/29/2017.
 * An implementation of AsyncTask class for getting Coordinate of an address.
 * This class will be used when new owner create new {@link com.coffeeteam.coffeeviet.entities.CoffeeShop} information
 * and push this new into Firebase database
 */

public class LocationRequestFromAddressAsync extends AsyncTask<String, Void, List<GooglePlaceWebServiceGeometry>> {

    // failed request result constant
    // This will be returned on doInBackground method if the work fails.
    public static final LatLng FAILED_REQUEST_RESULT = new LatLng(0.0D, 0.0D);

    public static final String REQUEST_URL_PREFIX = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="; //

    // TAG for logging
    private static final String TAG = LocationRequestFromAddressAsync.class.getSimpleName();

    private Context context; // context used for working with Geocode object

    /**
     * Member constructor
     *
     * @param context : {@link Context} of {@link android.app.Activity } using this class
     */
    public LocationRequestFromAddressAsync(Context context) {
        this.context = context;
    }


    /**
     * Do the request work
     *
     * @param strings :
     * @return :
     */
    @Override
    protected List<GooglePlaceWebServiceGeometry> doInBackground(String... strings) {
        List<GooglePlaceWebServiceGeometry> requestResultList = null;

        String searchString = strings[0]; // this is address string

        String completeRequestUrl = REQUEST_URL_PREFIX;
        try {
            completeRequestUrl += URLEncoder.encode(searchString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        completeRequestUrl += "&key=" + this.context.getString(R.string.google_place_api_key);

        Log.d(TAG, "Request Url : " + completeRequestUrl);
        // start to connect by HttpUrlConnection
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(completeRequestUrl); // make sure not whitespace in url
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(3000);
            httpURLConnection.setConnectTimeout(3000);
            httpURLConnection.setDoInput(true);
            // Already true by default but setting just in case; needs to be true since this request
            // is carrying an input (response) body.
            httpURLConnection.connect();
            String resultString = this.readStringFromHttpUrlConnection(httpURLConnection);
            Log.d(TAG, "string web service : " + resultString);
            JSONObject jsonObject = new JSONObject(resultString);
            requestResultList = JsonParserUtil.parseGooglePlaceWebServiceGeometryJsonResult(jsonObject);
        } catch (IOException | JSONException e) {
            Log.e(TAG, e.getMessage(), e);
            e.printStackTrace();
            requestResultList = new ArrayList<>(); // return empty list, not null value
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        return requestResultList;
    }

    @Override
    protected void onPostExecute(List<GooglePlaceWebServiceGeometry> results) {
        // log to see the result
        for (GooglePlaceWebServiceGeometry e :
                results) {
            Log.d(TAG, e.toString());
        }
    }


    /**
     * Read {@link String} content from {@link java.io.InputStream}.
     *
     * @param httpURLConnection : {@link HttpURLConnection} reference to get {@link java.io.InputStream} from.
     * @return : string content for later process such as Json Parsing
     * @throws IOException if error occurs.
     */
    private String readStringFromHttpUrlConnection(HttpURLConnection httpURLConnection)
            throws IOException {
        InputStreamReader isr =
                new InputStreamReader(httpURLConnection.getInputStream());
        BufferedReader br = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append('\n');
        }
        return sb.toString();
    }
}
