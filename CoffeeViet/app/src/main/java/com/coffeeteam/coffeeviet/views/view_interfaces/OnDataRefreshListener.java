package com.coffeeteam.coffeeviet.views.view_interfaces;

/**
 * Created by TNS on 5/17/2017.
 * Interface for handling to signal fragments for freshing data from firebase or local database.
 * Fragments that needs signaling from {@link android.app.Activity} to fresh will implement this interface.
 */

public interface OnDataRefreshListener {

    /**
     * Refresh firebase data.
     * This method should be invoke from {@link com.coffeeteam.coffeeviet.managers.activities.MainActivity}
     * to signal fragment to refresh data
     */
    void refreshFirebaseData();

    /**
     * Refresh local database.
     */
    void refreshLocalDatabase();

    /**
     * Device's connection state is not available,
     * so start to do offline work
     */
    void doOfflineWork();
}
