package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public interface IEventSchema {
    public static final String EVENT_TABLE = "EVENT";

    public static final String ID = "_id";
    public static final String EVENT_ID = "id";
    public static final String EVENT_NAME = "name";
    public static final String EVENT_COFFEE_SHOP_ID = "coffee_shop_id";
    public static final String EVENT_DESCRIPTION = "description";
    public static final String EVENT_THEME_PHOTO = "theme_photo";
    public static final String EVENT_INTERESTS = "interests";
    public static final String EVENT_GOING = "going";
    public static final String EVENT_JOIN = "joins";
    public static final String EVENT_COMMENT = "comment";
    public static final String EVENT_MEMBER_NAME = "member_name";

    public static final String EVENT_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + EVENT_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + EVENT_ID
            + " TEXT NOT NULL, "
            + EVENT_NAME
            + " TEXT NOT NULL, "
            + EVENT_COFFEE_SHOP_ID
            + " TEXT, "
            + EVENT_DESCRIPTION
            + " TEXT, "
            + EVENT_THEME_PHOTO
            + " TEXT, "
            + EVENT_INTERESTS
            + " INT, "
            + EVENT_GOING
            + " INT, "
            + EVENT_JOIN
            + " INT, "
            + EVENT_COMMENT
            + " INT, "
            + EVENT_MEMBER_NAME
            + " TEXT"
            + ")";

    public static final String[] EVENT_COLUMNS = new String[]{ID,
            EVENT_ID, EVENT_NAME, EVENT_COFFEE_SHOP_ID, EVENT_DESCRIPTION, EVENT_THEME_PHOTO, EVENT_INTERESTS,
            EVENT_GOING, EVENT_JOIN, EVENT_COMMENT, EVENT_MEMBER_NAME};
}
