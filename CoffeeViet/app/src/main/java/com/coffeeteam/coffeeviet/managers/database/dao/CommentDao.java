package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.coffeeteam.coffeeviet.entities.Comment;
import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.ICommentDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.ICommentSchema;

import java.util.List;

/**
 * Created by Hau-Do on 21/03/2017.
 */

public class CommentDao extends DbContentProvider implements ICommentSchema, ICommentDao {

    private Cursor mCursor;
    private List<Comment> mCommentList;
    private ContentValues mInitialValues;

    public CommentDao(SQLiteDatabase db) {
        super(db);
    }


    @Override
    public Comment fetchCommentById(int id) {
        return null;
    }

    @Override
    public List<Comment> fetchAllComments() {
        return null;
    }

    @Override
    public boolean addComment(Comment comment) {
        return false;
    }

    @Override
    public boolean addComment(List<Comment> commentList) {
        return false;
    }

    @Override
    public boolean deleteAllComments() {
        return false;
    }

    @Override
    protected <T> T cursorToEntity(Cursor cursor) {
        return null;
    }
}
