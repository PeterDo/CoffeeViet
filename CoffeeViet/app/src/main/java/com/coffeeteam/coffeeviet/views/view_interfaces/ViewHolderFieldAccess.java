package com.coffeeteam.coffeeviet.views.view_interfaces;

import android.support.v7.widget.CardView;

/**
 * Created by TNS on 4/11/2017.
 */

public interface ViewHolderFieldAccess {
    /**
     * Get {@link CardView} object of an custom {@link android.support.v7.widget.RecyclerView.ViewHolder}.
     *
     * @return : specified {@link CardView} of {@link android.support.v7.widget.RecyclerView.ViewHolder},
     * otherwise, returns null if view holder does not
     * have {@link CardView} object.
     */
    CardView getCardView();

    /**
     * Get value shouldAnimate for {@link android.support.v7.widget.RecyclerView.ViewHolder}
     *
     * @return : shouldAnimate value
     */
    public boolean getShouldAnimateValue();

    /**
     * Set value shouldAnimate for {@link android.support.v7.widget.RecyclerView.ViewHolder}
     *
     * @param value
     */
    void shouldAnimate(boolean value);

}
