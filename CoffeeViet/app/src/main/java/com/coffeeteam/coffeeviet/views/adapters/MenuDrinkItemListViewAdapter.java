package com.coffeeteam.coffeeviet.views.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.MenuItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TNS on 5/13/2017.
 */

public class MenuDrinkItemListViewAdapter extends ArrayAdapter<MenuItem> {

    public static final String TAG = MenuDrinkItemListViewAdapter.class.getSimpleName(); // tag
    private Context context;
    private List<MenuItem> listOfMenuItems;

    public MenuDrinkItemListViewAdapter(@NonNull Context context,
                                        @LayoutRes int resource, @NonNull List<MenuItem> objects) {
        super(context, resource, objects);
        this.context = context;
        this.listOfMenuItems = objects;
    }


    public static class ViewHolder {
        public ImageView imageViewMenuItem;
        public TextView textViewMenuItemName;
        public TextView textViewMenuItemPrice;


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        // make sure convertView is valid
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.coffee_shop_menu_list_item,
                    parent, false);
            //start to bind view
            viewHolder = new ViewHolder();
            viewHolder.imageViewMenuItem = (ImageView) convertView.findViewById(R.id.imageview_menu_item);
            viewHolder.textViewMenuItemName = (TextView) convertView.findViewById(R.id.textview_menu_item_name);
            viewHolder.textViewMenuItemPrice = (TextView) convertView.findViewById(R.id.textview_menu_item_price);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // get current Menu item from list and bind value here.................
        MenuItem currentMenuItem = this.listOfMenuItems.get(position);
        viewHolder.textViewMenuItemName.setText(currentMenuItem.getName());
        viewHolder.textViewMenuItemPrice.setText(currentMenuItem.getPrice() + "");
        // if current item is drink
        viewHolder.imageViewMenuItem.setImageResource(R.drawable.icon_drink);
        return convertView;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Nullable
    @Override
    public MenuItem getItem(int position) {
        return super.getItem(position);
    }
}
