package com.coffeeteam.coffeeviet.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class Event implements Parcelable {
    private int _id;
    private String id;
    private String name;
    private String coffee_shop_id;
    private String description;
    private List<String> theme_photo;
    private int interestCount;
    private int goingCount;
    private int joinCount;
    private int comment;
    private String member_name;
    private String start_date;
    private String start_time;
    private String end_date;
    private String end_time;

    public Event() {
    }

    protected Event(Parcel in) {
        _id = in.readInt();
        id = in.readString();
        name = in.readString();
        coffee_shop_id = in.readString();
        description = in.readString();
        theme_photo = in.createStringArrayList();
        interestCount = in.readInt();
        goingCount = in.readInt();
        joinCount = in.readInt();
        comment = in.readInt();
        member_name = in.readString();
        start_date = in.readString();
        start_time = in.readString();
        end_date = in.readString();
        end_time = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoffee_shop_id() {
        return coffee_shop_id;
    }

    public void setCoffee_shop_id(String coffee_shop_id) {
        this.coffee_shop_id = coffee_shop_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getTheme_photo() {
        return theme_photo;
    }

    public void setTheme_photo(List<String> theme_photo) {
        this.theme_photo = theme_photo;
    }

    public int getInterestCount() {
        return interestCount;
    }

    public void setInterestCount(int interestCount) {
        this.interestCount = interestCount;
    }

    public int getGoingCount() {
        return goingCount;
    }

    public void setGoingCount(int goingCount) {
        this.goingCount = goingCount;
    }

    public int getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(int joinCount) {
        this.joinCount = joinCount;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(_id);
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(coffee_shop_id);
        parcel.writeString(description);
        parcel.writeStringList(theme_photo);
        parcel.writeInt(interestCount);
        parcel.writeInt(goingCount);
        parcel.writeInt(joinCount);
        parcel.writeInt(comment);
        parcel.writeString(member_name);
        parcel.writeString(start_date);
        parcel.writeString(start_time);
        parcel.writeString(end_date);
        parcel.writeString(end_time);
    }
}
