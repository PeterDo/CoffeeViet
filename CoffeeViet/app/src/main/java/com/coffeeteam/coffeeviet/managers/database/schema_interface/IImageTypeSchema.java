package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public interface IImageTypeSchema {
    public static final String IMAGE_TYPE_TABLE = "IMAGE_TYPE";

    public static final String ID = "_id";
    public static final String IMAGE_TYPE_SHOP_THEME = "shop_theme_list";
    public static final String IMAGE_TYPE_EVENT_THEME = "event_them_list";
    public static final String IMAGE_TYPE_COFFEE_SHOP_ID = "coffee_shop_id";

    public static final String IMAGE_TYPE_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + IMAGE_TYPE_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + IMAGE_TYPE_SHOP_THEME
            + " TEXT, "
            + IMAGE_TYPE_EVENT_THEME
            + " TEXT, "
            + IMAGE_TYPE_COFFEE_SHOP_ID
            + " TEXT"
            + ")";

    public static final String[] IMAGE_TYPE_COLUMNS = new String[]{ID,
            IMAGE_TYPE_SHOP_THEME, IMAGE_TYPE_EVENT_THEME, IMAGE_TYPE_COFFEE_SHOP_ID};
}
