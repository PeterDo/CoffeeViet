package com.coffeeteam.coffeeviet.views.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.User;
import com.coffeeteam.coffeeviet.entities.UserTut;
import com.coffeeteam.coffeeviet.managers.activities.UserProfileActivity;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TNS on 3/17/2017.
 */

public class FriendListCardAdapter extends RecyclerView.Adapter<FriendListCardAdapter.ViewHolder> {

    private Activity activity;
    private Context context; // context of adapter
    private List<User> listOfFriends;


    public FriendListCardAdapter(Context context, List<User> listOfUsers) {
        this.context = context;
        this.listOfFriends = listOfUsers;
    }

    public FriendListCardAdapter(Activity activity, List<User> listOfUsers) {
        this.activity = activity;
        this.context = activity.getBaseContext();
        this.listOfFriends = listOfUsers;
    }

    /**
     * Bind view layout from resource
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public FriendListCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friend_list_item, parent, false);

        return new FriendListCardAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FriendListCardAdapter.ViewHolder holder, int position) {
        // get coffee shop at index is position as second parameter
        final User currentUser = this.listOfFriends.get(position);
        // get first character
        char prefix = currentUser.getUsername().charAt(0);
        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .width(120)  // width in px
                .height(120) // height in px
                .endConfig()
                .buildRect(String.valueOf(prefix), ColorGenerator.MATERIAL.getRandomColor());
        // start to set up view values for ViewHolder
        holder.circleImageViewActiveFriendProfilePhoto.setImageDrawable(textDrawable);

        holder.circleImageViewActiveFriendProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // make sure we have valid activity instance
                if (activity != null) {
                    Intent userProfileIntent = new Intent(activity, UserProfileActivity.class);
                    // add intent values
                    userProfileIntent.putExtra(PrefsUtil.SELECTED_USER, currentUser);
                    final View transientView = holder.circleImageViewActiveFriendProfilePhoto;
                    // make transient when move to user profile detail activity
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        activity.startActivity(userProfileIntent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
                    } else {
                        activity.startActivity(userProfileIntent);
                    }
                } else {
                    Intent userProfileIntent = new Intent(context, UserProfileActivity.class);
                    context.startActivity(userProfileIntent);
                }
            }
        });
        // text for testing
        holder.textViewFriendName.setText(currentUser.getUsername());
        holder.textViewFriendLastLocation.setText(currentUser.getLast_location_string());
        holder.textViewFriendLastActiveTime.setText(this.context.getString(R.string.sample_last_active_time_1));

        // TEST VIEW VISIBILITY
        Random random = new Random();
        if (random.nextInt() % 2 == 0) {
            holder.circleImageViewActive.setVisibility(View.VISIBLE);
            holder.textViewFriendLastActiveTime.setVisibility(View.GONE);
        } else {
            holder.textViewFriendLastActiveTime.setVisibility(View.VISIBLE);
            holder.circleImageViewActive.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return this.listOfFriends.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item will contain coffee shop information
        public CircleImageView circleImageViewActiveFriendProfilePhoto;
        public TextView textViewFriendName;
        public TextView textViewFriendLastLocation;
        public CircleImageView circleImageViewActive; // will be shown if current friend is active
        public TextView textViewFriendLastActiveTime;

        // member constructor with parameter is view containing all child elements
        public ViewHolder(View view) {
            super(view);
            // start to bind view
            this.circleImageViewActiveFriendProfilePhoto = (CircleImageView) view.findViewById(R.id.circle_imageview_friend_photo);
            this.circleImageViewActive = (CircleImageView) view.findViewById(R.id.circle_imageview_active);
            this.textViewFriendName = (TextView) view.findViewById(R.id.textview_friend_name);
            this.textViewFriendLastLocation = (TextView) view.findViewById(R.id.textview_friend_last_location);
            this.textViewFriendLastActiveTime = (TextView) view.findViewById(R.id.textview_last_active_time);
        }
    }
}
