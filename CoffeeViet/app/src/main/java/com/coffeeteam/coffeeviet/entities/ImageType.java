package com.coffeeteam.coffeeviet.entities;

import java.util.List;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class ImageType {
    private int _id;
    private List<String> shop_theme_list;
    private List<String> event_theme_list;
    private String coffee_shop_id;

    public ImageType() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public List<String> getShop_theme_list() {
        return shop_theme_list;
    }

    public void setShop_theme_list(List<String> shop_theme_list) {
        this.shop_theme_list = shop_theme_list;
    }

    public List<String> getEvent_them_list() {
        return event_theme_list;
    }

    public void setEvent_them_list(List<String> event_theme_list) {
        this.event_theme_list = event_theme_list;
    }

    public String getCoffee_shop_id() {
        return coffee_shop_id;
    }

    public void setCoffee_shop_id(String coffee_shop_id) {
        this.coffee_shop_id = coffee_shop_id;
    }
}
