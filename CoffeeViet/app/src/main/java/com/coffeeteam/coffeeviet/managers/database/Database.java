package com.coffeeteam.coffeeviet.managers.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.coffeeteam.coffeeviet.managers.database.dao.CoffeeShopDao;
import com.coffeeteam.coffeeviet.managers.database.dao.UserTutTutDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.IUserTutSchema;

/**
 * Created by Hau-Do on 15/03/2017.
 */

/**
 * provide an handle to the database resource
 */
public class Database {

    private static final String TAG = "CoffeeDatabase";
    private static final String DATABASE_NAME = "coffee_database.db";
    private DatabaseHelper mDbHelper;
    private static final int DATABASE_VERSION = 1;
    private final Context mContext;

    public static UserTutTutDao mUserDao;
    public static CoffeeShopDao mCoffeeShopDao;

    public Database(Context context) {
        this.mContext = context;
    }


    public Database open() throws SQLException {
        mDbHelper = new DatabaseHelper(mContext);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        mUserDao = new UserTutTutDao(mDb);
        mCoffeeShopDao = new CoffeeShopDao(mDb);

        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    /**
     * Inner Class
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(IUserTutSchema.USER_TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion) {
            Log.w(TAG, "Upgrading database from version "
                    + oldVersion + " to "
                    + newVersion + " which destroys all old data");

            db.execSQL("DROP TABLE IF EXISTS "
                    + IUserTutSchema.USER_TABLE);
            onCreate(db);

        }
    }

}
