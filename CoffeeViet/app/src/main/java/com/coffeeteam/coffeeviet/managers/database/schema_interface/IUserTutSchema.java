package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 15/03/2017.
 */

/**
 * USER schema definition
 */
public interface IUserTutSchema {
    public static final String USER_TABLE = "USER";

    public static final String ID = "_id";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "email";
    public static final String USER_DATE = "created_date";

    public static final String USER_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + USER_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + USER_ID
            + " INTEGER, "
            + USER_NAME
            + " TEXT NOT NULL, "
            + USER_EMAIL
            + " TEXT,"
            + USER_DATE
            + " BIGINT"
            + ")";

    public static final String[] USER_COLUMNS = new String[]{ID,
            USER_ID, USER_NAME, USER_EMAIL, USER_DATE};
}
