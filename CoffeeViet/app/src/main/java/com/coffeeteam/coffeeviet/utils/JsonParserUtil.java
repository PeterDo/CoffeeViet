package com.coffeeteam.coffeeviet.utils;

import android.util.Log;

import com.coffeeteam.coffeeviet.entities.GooglePlaceWebServiceGeometry;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by TNS on 5/24/2017.
 */

public class JsonParserUtil {

    private static final String TAG = JsonParserUtil.class
            .getSimpleName();

    private JsonParserUtil() {

    }


    /**
     * @param jsonResult : {@link JSONObject} from web service
     * @return : {@link List} of {@link GooglePlaceWebServiceGeometry} for desired request
     */
    public static List<GooglePlaceWebServiceGeometry> parseGooglePlaceWebServiceGeometryJsonResult(JSONObject jsonResult)
            throws JSONException {

        Log.d(TAG, "begin reading !");
        // return list
        List<GooglePlaceWebServiceGeometry> returnList = new ArrayList<>();
        JSONArray results = jsonResult.getJSONArray("results");

        GooglePlaceWebServiceGeometry temporaryHolder;
        for (int i = 0; i < results.length(); ++i) {
            temporaryHolder = new GooglePlaceWebServiceGeometry();
            JSONObject geometryJsonObject = results.getJSONObject(i);
            temporaryHolder.setLatitude(geometryJsonObject.getJSONObject("geometry").getJSONObject("location").getDouble("lat"));
            temporaryHolder.setLongitude(geometryJsonObject.getJSONObject("geometry").getJSONObject("location").getDouble("lng"));
            temporaryHolder.setIconUrl(geometryJsonObject.getString("icon"));
            temporaryHolder.setPlaceName(geometryJsonObject.getString("name"));
            //     temporaryHolder.setRating(geometryJsonObject.getDouble("rating"));
            temporaryHolder.setFormattedAddress(geometryJsonObject.getString("formatted_address"));

            // get all opt fields. This means this can exist in some response
            // but in others.
            temporaryHolder.setRating(geometryJsonObject.optDouble("rating"));
            // add into return list
            returnList.add(temporaryHolder);
        }

        return returnList;
    }


    /**
     * Parse {@link JSONObject} for getting result from Google Direction WebService to
     * get list of instructions to drive between 2 location (origin and destination).
     * For Google Direction API for web service documentation,
     * visit : https://developers.google.com/maps/documentation/directions/intro#Waypoints
     * This method is referenced from  tutorial : http://wptrafficanalyzer.in/blog/drawing-driving-route-directions-between-two-locations-using-google-directions-in-google-map-android-api-v2/
     *
     * @param jObject
     * @return
     * @throws JSONException
     */
    public static List<List<HashMap<String, String>>> parseGoogleWayPointsWebService(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<>();
        JSONArray jRoutes;
        JSONArray jLegs;
        JSONArray jSteps;

        try {

            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("lat", Double.toString(list.get(l).latitude));
                            hm.put("lng", Double.toString(list.get(l).longitude));
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }

        return routes;
    }


    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private static List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

}
