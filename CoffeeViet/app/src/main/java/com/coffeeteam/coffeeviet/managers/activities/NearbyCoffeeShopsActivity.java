package com.coffeeteam.coffeeviet.managers.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.entities.GooglePlaceWebServiceGeometry;
import com.coffeeteam.coffeeviet.managers.customs.LatLngInterpolator;
import com.coffeeteam.coffeeviet.utils.JsonParserUtil;
import com.coffeeteam.coffeeviet.utils.MarkerAnimation;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NearbyCoffeeShopsActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapClickListener {

    // Define Action types for activity

    // for direct user from selected coffee shop navigating from detail screen
    public static final int ACTION_FIREBASE_COFFEE_SHOP_DETAIL_DIRECTION = 1;
    // flag navigated from Main Activity
    public static final int ACTION_LOAD_NEAR_BY_COFFEE_SHOP = 2;
    // Intent key
    public static String MAP_ACTION_TYPE = "map_action_type";

    private static final String TAG = NearbyCoffeeShopsActivity.class.getSimpleName();
    private static final float DEFAULT_ZOOM_VALUE = 13; // default zoom for Google Map object
    private static final int POLYLINE_STROKE_WIDTH_PX = 12;

    private GoogleMap mMap;
    private Polyline routePolyline; // polyline for guiding user on map
    private DatabaseReference databaseReference;
    private Query query;

    private RelativeLayout instantInformationView; // hold reference to instant information
    private int actionType; // action type for Activity
    private String detailCoffeeShopLatitude;
    private String detailCoffeeShopLongitude;
    private String detailCoffeeShopName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_nearby_coffee_shops);
        // set up toolbar
        Toolbar toolbar = (Toolbar) this.findViewById(R.id.map_toolbar);
        this.setSupportActionBar(toolbar);
        // set home button as up
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // initialize database
        this.databaseReference = FirebaseDatabase.getInstance().getReference();

        // get action types from Intent
        Intent intent = this.getIntent();
        this.actionType = intent.getIntExtra(MAP_ACTION_TYPE,
                ACTION_LOAD_NEAR_BY_COFFEE_SHOP // if invalid, load all recommendations and nearby shops by default
        );
        // get location information from Intent. NOTE : this may be null
        this.detailCoffeeShopLongitude = intent.getStringExtra(CoffeeShopDetail.DETAIL_COFFEE_SHOP_LONGITUDE);
        this.detailCoffeeShopLatitude = intent.getStringExtra(CoffeeShopDetail.DETAIL_COFFEE_SHOP_LATITUDE);
        this.detailCoffeeShopName = intent.getStringExtra(CoffeeShopDetail.DETAIL_COFFEE_SHOP_NAME);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // base on action type, we back to corresponding top stack Activity
                if (this.actionType == ACTION_LOAD_NEAR_BY_COFFEE_SHOP) {
                    Intent upIntent = NavUtils.getParentActivityIntent(this);
                    if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                        // This activity is NOT part of this app's task, so create a new task
                        // when navigating up, with a synthesized back stack.
                        TaskStackBuilder.create(this)
                                // Add all of this activity's parents to the back stack
                                .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                                .startActivities();
                    } else {
                        // This activity is part of this app's task, so simply
                        // navigate up to the logical parent activity.
                        NavUtils.navigateUpTo(this, upIntent);
                    }
                } else if (this.actionType == ACTION_FIREBASE_COFFEE_SHOP_DETAIL_DIRECTION) {
                    // this means the top stack Activity is CoffeeShopDetail
                    // so we back to this Activity, not MainActivity specified on Manifest.xml file
                    this.onBackPressed();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap; // mark the map object

        if (PrefsUtil.getBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_PERMISSION_GRANTED)) {

            this.mMap.setMyLocationEnabled(true);
            this.mMap.getUiSettings().setMyLocationButtonEnabled(true);
            // Disable Map Toolbar buttons
            mMap.getUiSettings().setMapToolbarEnabled(false);
            // map click listener
            this.mMap.setOnMapClickListener(this);

            // move camera to device's current location
            String currentLatitude = PrefsUtil.getStringPrefValue(
                    this.getApplicationContext(),
                    PrefsUtil.DEVICE_CURRENT_LOCATION_LATITUDE);
            String currentLongitude = PrefsUtil.getStringPrefValue(
                    this.getApplicationContext(),
                    PrefsUtil.DEVICE_CURRENT_LOCATION_LONGITUDE);
            // make sure all location preference values are valid
            if (!currentLatitude.equals(PrefsUtil.PREF_DEFAULT_VALUE) &&
                    !currentLongitude.equals(PrefsUtil.PREF_DEFAULT_VALUE)) {
                LatLng currentLatLng = new LatLng(
                        Double.parseDouble(currentLatitude),
                        Double.parseDouble(currentLongitude)
                );
                // zoom camera
                this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_ZOOM_VALUE));
            }

            // do different work based on action type
            if (this.actionType == ACTION_LOAD_NEAR_BY_COFFEE_SHOP) {

                // load all data from firebase related to user current location and Machine Learning for
                // loading recommended coffee shops to user
                this.markingRecommendedCoffeeShopsToGoogleMap(mMap);
                // use Google Place API Web service to get nearby coffee shops asynchronously
                FetchNearbyCoffeeShopsGooglePlacesAsync fetchNearbyCoffeeShopsGooglePlacesAsync
                        = new FetchNearbyCoffeeShopsGooglePlacesAsync(this.mMap);
                fetchNearbyCoffeeShopsGooglePlacesAsync.execute();
                // Google Map marker information window listener
                this.mMap.setOnInfoWindowClickListener(this);

            } else if (this.actionType == ACTION_FIREBASE_COFFEE_SHOP_DETAIL_DIRECTION) {
                // add marker at selected detail coffee shop
                Bitmap cafeMapIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_map_origin);
                cafeMapIcon = Bitmap.createScaledBitmap(cafeMapIcon, 56, 56, false);
                LatLng latLng = new LatLng(Double.parseDouble(this.detailCoffeeShopLatitude),
                        Double.parseDouble(this.detailCoffeeShopLongitude));
                Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
                        .title(this.detailCoffeeShopName)
                        .icon(BitmapDescriptorFactory.fromBitmap(cafeMapIcon)));
                // show instant information view if user click the title
                marker.setTag(this.detailCoffeeShopName);
                // show title always
                marker.showInfoWindow();
                // start marker animation
                MarkerAnimation.animateMarkerToHC(marker, latLng, new LatLngInterpolator.Spherical());
                // this activity starts from CoffeeShopDetail, so start to get selected detail coffee shop location
                // from Intent
                // first , check if Intent parameter's values are valid
                if (this.detailCoffeeShopLatitude != null && this.detailCoffeeShopLongitude != null && this.detailCoffeeShopName != null) {
                    DrawPolylineAsync drawPolylineAsync = new DrawPolylineAsync(this.mMap,
                            currentLatitude + "," + currentLongitude, // origin
                            this.detailCoffeeShopLatitude + "," + this.detailCoffeeShopLongitude // destination
                    );

                    drawPolylineAsync.execute();
                }
            }

        }
    }


    /**
     * Base on device's current location.
     * Application will load all relevant coffee shops and collaborate with Machine Learning
     * to find all recommended coffee shops
     */

    private void markingRecommendedCoffeeShopsToGoogleMap(final GoogleMap googleMap) {
        // make sure we have valid DatabaseReference
        if (this.databaseReference != null) {
            this.query = this.databaseReference.child("shops");
            this.query.addValueEventListener(new ValueEventListener() {
                List<CoffeeShop> listOfCoffeeShops = new ArrayList<>();

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                        //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                        CoffeeShop coffeeShop = shopDataSnapshot.getValue(CoffeeShop.class);
                        coffeeShop.setFirebaseKey(shopDataSnapshot.getKey()); // get database Fire base
                        listOfCoffeeShops.add(coffeeShop);
                        Log.d(TAG, "CoffeeShop information : " + coffeeShop.toString());
                    }

                    Bitmap cafeMapIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_map_origin);
                    cafeMapIcon = Bitmap.createScaledBitmap(cafeMapIcon, 56, 56, false);

                    // marking to GoogleMap
                    if (!listOfCoffeeShops.isEmpty()) {
                        for (CoffeeShop shop :
                                listOfCoffeeShops) {
                            if (shop.getLatitude() != null && shop.getLongitude() != null) {
                                LatLng latLng = new LatLng(shop.getLatitude(), shop.getLongitude());
                                Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
                                        .title(shop.getName())
                                        .icon(BitmapDescriptorFactory.fromBitmap(cafeMapIcon)));
                                marker.setTag(shop.getFirebaseKey());
                                // show title always
                                marker.showInfoWindow();

                                // start marker animation
                                MarkerAnimation.animateMarkerToGB(marker, latLng, new LatLngInterpolator.Linear());
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // see detail of selected coffee shop (marker) if available
        Log.d(TAG, "onInfoWindowClick : " + marker.toString());
        //
        if (marker.getTag() instanceof GooglePlaceWebServiceGeometry) {
            // show instant view for displaying short detail of selected coffee shop
            // which is result from requesting from Google Place API Web service
            this.showInstantCoffeeShopInformationView((GooglePlaceWebServiceGeometry) (marker.getTag()));
        } else {
            // get tag (firebase key)
            final String fireBaseKey = (String) marker.getTag();
            if (fireBaseKey != null && !fireBaseKey.isEmpty()) {
                this.query = this.databaseReference.child("shops").child(fireBaseKey);
                this.query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        CoffeeShop selectedCoffeeShop = snapshot.getValue(CoffeeShop.class);
                        selectedCoffeeShop.setFirebaseKey(snapshot.getKey()); // get database Fire base
                        Intent coffeeShopDetailIntent = new Intent(getApplicationContext(), CoffeeShopDetail.class);
                        Bundle bundle = new Bundle();
                        // put bundle values
                        bundle.putParcelable(PrefsUtil.CURRENT_SHOP, selectedCoffeeShop);
                        coffeeShopDetailIntent.putExtras(bundle);
                        startActivity(coffeeShopDetailIntent);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        }
    }

    /**
     * Handle when user click the {@link GoogleMap} object
     *
     * @param latLng : Location in {@link LatLng} to handle with
     */
    @Override
    public void onMapClick(LatLng latLng) {
        // hide the instant view

    }


    /**
     * Implementation of AsyncTask for getting result from Google Place API
     * results of all nearby coffee shops based on device's current location.
     * This class will be use after {@link GoogleMap} is initialized.
     */
    private class FetchNearbyCoffeeShopsGooglePlacesAsync
            extends AsyncTask<Void, Void, List<GooglePlaceWebServiceGeometry>> {
        // GoogleMap object for add Marker
        private GoogleMap googleMap;


        private FetchNearbyCoffeeShopsGooglePlacesAsync(GoogleMap googleMap) {
            this.googleMap = googleMap;
        }


        /**
         * Do the job in background.
         * This includes request Google place API Webservice to get all nearby coffee shops in 3km radius.
         *
         * @param voids : Nothing input.
         * @return List of {@link GooglePlaceWebServiceGeometry}
         */
        @Override
        protected List<GooglePlaceWebServiceGeometry> doInBackground(Void... voids) {
            List<GooglePlaceWebServiceGeometry> requestResultList = null;
            String currentLatitude = PrefsUtil.getStringPrefValue(getApplicationContext(),
                    PrefsUtil.DEVICE_CURRENT_LOCATION_LATITUDE);
            String currentLongitude = PrefsUtil.getStringPrefValue(getApplicationContext(),
                    PrefsUtil.DEVICE_CURRENT_LOCATION_LONGITUDE);
            // make sure current location is valid
            if (!currentLatitude.equals(PrefsUtil.PREF_DEFAULT_VALUE)
                    && !currentLongitude.equals(PrefsUtil.PREF_DEFAULT_VALUE)) {
                String currentLocationPairString = currentLatitude + "," + currentLongitude;
                String requestUrl = prepareGooglePlaceApiWebServiceRequestUrl(
                        "", // empty search string
                        "cafe", // search type is cafe
                        currentLocationPairString, // location pair string
                        3000 // radius is 3km
                );

                // start to connect by HttpUrlConnection
                HttpURLConnection httpURLConnection = null;
                try {
                    URL url = new URL(requestUrl);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setReadTimeout(3000);
                    httpURLConnection.setConnectTimeout(3000);
                    httpURLConnection.setDoInput(true);
                    // Already true by default but setting just in case; needs to be true since this request
                    // is carrying an input (response) body.
                    httpURLConnection.connect();
                    String resultString = readStringFromHttpUrlConnection(httpURLConnection);
                    Log.d(TAG, "string web service : " + resultString);
                    JSONObject jsonObject = new JSONObject(resultString);
                    requestResultList = JsonParserUtil.parseGooglePlaceWebServiceGeometryJsonResult(jsonObject);
                } catch (IOException | JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                    e.printStackTrace();
                    requestResultList = new ArrayList<>(); // return empty list, not null value
                } finally {
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                }
            }
            Log.d(TAG, "End reading !");
            return requestResultList;
        }

        /**
         * The job done.
         *
         * @param googlePlaceWebServiceGeometries : Result of the job
         */
        @Override
        protected void onPostExecute(List<GooglePlaceWebServiceGeometry> googlePlaceWebServiceGeometries) {
            super.onPostExecute(googlePlaceWebServiceGeometries);
            Log.d(TAG, "onPostExecute, result's size :  " + googlePlaceWebServiceGeometries.size());
            // add marker into Google Map after fetching web service result
            if (this.googleMap != null && !googlePlaceWebServiceGeometries.isEmpty()) {
                //
                Bitmap cafeMapIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_map_origin);
                cafeMapIcon = Bitmap.createScaledBitmap(cafeMapIcon, 56, 56, false);
                for (GooglePlaceWebServiceGeometry geo :
                        googlePlaceWebServiceGeometries) {
                    LatLng latLng = new LatLng(geo.getLatitude(), geo.getLongitude());
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
                            .title(geo.getPlaceName())
                            .icon(BitmapDescriptorFactory.fromBitmap(cafeMapIcon)));
                    //marker.setTag(shop.getFirebaseKey());
                    // show instant information view if user click the title
                    marker.setTag(geo);
                    // show title always
                    marker.showInfoWindow();
                    // start marker animation
                    MarkerAnimation.animateMarkerToHC(marker, latLng, new LatLngInterpolator.Spherical());
                }
            }

        }
    }


    /**
     * Class to process Google Direction Request from client and then
     * draw polyline result on {@link GoogleMap} object
     */
    private class DrawPolylineAsync extends AsyncTask<Void, Void, List<List<HashMap<String, String>>>> {
        private GoogleMap googleMap;
        private String origin;
        private String destination;


        /**
         * Member constructor
         *
         * @param googleMap   : {@link GoogleMap} reference
         * @param origin      : origin location pair string. For example (1232,43433)
         * @param destination : destination pair string. For example (1232,43433)
         */
        public DrawPolylineAsync(GoogleMap googleMap, String origin, String destination) {
            this.googleMap = googleMap;
            this.origin = origin;
            this.destination = destination;
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(Void... voids) {
            List<List<HashMap<String, String>>> listOfSteps; // list to be returned
            // create url string
            String urlString = prepareGoogleWayPointsWebServiceRequestUrl(this.origin, this.destination);
            Log.d(TAG, "Google Direction request url : " + urlString);
            HttpURLConnection httpURLConnection = null;
            try {
                URL url = new URL(urlString);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setReadTimeout(3000);
                httpURLConnection.setConnectTimeout(3000);
                httpURLConnection.setDoInput(true);
                // Already true by default but setting just in case; needs to be true since this request
                // is carrying an input (response) body.
                httpURLConnection.connect();
                String resultString = readStringFromHttpUrlConnection(httpURLConnection);
                Log.d(TAG, "google direction API string web service : " + resultString);
                JSONObject googleDirectionJsonResult = new JSONObject(resultString);
                // get list of steps (locations) from JSON result
                listOfSteps = JsonParserUtil.parseGoogleWayPointsWebService(googleDirectionJsonResult);
            } catch (JSONException | IOException e) {
                e.printStackTrace();
                listOfSteps = new ArrayList<>(); // return empty , not null
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return listOfSteps;
        }

        /**
         * The work done, start to draw route on {@link GoogleMap} object
         * {@link LatLng}
         *
         * @param result : list of {@link List<HashMap<String, String>>} parsed from JSON result
         */
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            // first, make sure we have valid objects to interact
            if (this.googleMap != null && !result.isEmpty()) {
                // start to draw poly lines into Map object
                ArrayList<LatLng> points;
                PolylineOptions lineOptions = null;

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    // styles the lines
                    lineOptions.width(POLYLINE_STROKE_WIDTH_PX);
                    lineOptions.color(Color.parseColor(getString(R.color.polyline_color)));
                    lineOptions.endCap(new RoundCap());
                    lineOptions.jointType(JointType.ROUND);
                }

                // Drawing polyline in the Google Map for the i-th route
                drawPolyLines(lineOptions);
            }
        }
    }

    /**
     * For directing user to coffee shop by Google Maps, this method will be used for drawing route
     * as direction to guide user.
     * This method will be invoked in 2 cases :
     * + From {@link CoffeeShopDetail} Activity
     * + From direct button of Instant Coffee Shop detail view.
     *
     * @param polylineOptions :  {@link PolylineOptions} that will
     */
    private void drawPolyLines(PolylineOptions polylineOptions) {
        // make sure we have valid google map object and option
        if (this.mMap != null && polylineOptions != null) {
            // remove all old poly lines
            if (this.routePolyline != null) {
                this.routePolyline.remove();
                this.routePolyline = null;
            }

            // start to redraw
            this.routePolyline = this.mMap.addPolyline(polylineOptions);
        }
    }


    /**
     * Show instant for user to see more details of unavailable data
     * from application Firebase data.
     *
     * @param shortDetail : {@link GooglePlaceWebServiceGeometry} result from Google Place API Web service to be displayed
     */
    private void showInstantCoffeeShopInformationView(final GooglePlaceWebServiceGeometry shortDetail) {
        Log.d(TAG, "showInstantCoffeeShopInformationView");
        final LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final RelativeLayout insertPoint = (RelativeLayout) findViewById(R.id.nearby_coffeeshops_viewgroup);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // make sure we have valid custom View before adding it into current ViewGroup
        if (this.instantInformationView == null) {
            this.instantInformationView = (RelativeLayout) vi.inflate(R.layout.coffee_shop_instant_infor_view, null);
        } else {
            // remove already view
            insertPoint.removeView(this.instantInformationView);
        }

        // set additional properties
        this.instantInformationView.setGravity(Gravity.BOTTOM);

        // prepare view content
        TextView instantCoffeeShopNameTextView = (TextView) this.instantInformationView.findViewById(R.id.instant_infor_shopname);
        TextView instantCoffeeShopAddressTextView = (TextView) this.instantInformationView.findViewById(R.id.instant_infor_shopaddress);
        TextView instantCoffeeShopRatingTextView = (TextView) this.instantInformationView.findViewById(R.id.instant_infor_rating);
        ImageButton closeInstantViewImageButton = (ImageButton) this.instantInformationView.findViewById(R.id.button_close_instant_view);
        closeInstantViewImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // close the instant view if use click close button
                // this means remove view from current ViewGroup
                insertPoint.removeView(instantInformationView);
                // toggle bool flag for inserting new View

            }
        });
        // direct me button
        Button buttonDirectMe = (Button) this.instantInformationView.findViewById(R.id.button_direct_me);

        buttonDirectMe.setOnClickListener(new View.OnClickListener() {
            /**
             * Draw Poly lines from device's current location to current selected Coffee Shop location.
             * This work will work asynchronously for application running smoothly
             * @param view
             */
            @Override
            public void onClick(View view) {
                // get device's current location from PrefsUtil
                String deviceCurrentLatitude = PrefsUtil.getStringPrefValue(getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LATITUDE);
                String deviceCurrentLongitude = PrefsUtil.getStringPrefValue(getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LONGITUDE);
                // make sure preference values are valid
                if (!deviceCurrentLatitude.equals(PrefsUtil.PREF_DEFAULT_VALUE) && !deviceCurrentLongitude.equals(PrefsUtil.PREF_DEFAULT_VALUE)) {


                    DrawPolylineAsync drawPolylineAsync = new DrawPolylineAsync(mMap,  // Map to be drawn in
                            deviceCurrentLatitude + "," + deviceCurrentLongitude, // origin
                            shortDetail.getLatitude() + "," + shortDetail.getLongitude() // destination
                    );
                    drawPolylineAsync.execute();
                }
            }
        });

        instantCoffeeShopNameTextView.setText(shortDetail.getPlaceName());
        instantCoffeeShopAddressTextView.setText(shortDetail.getFormattedAddress());
        if (shortDetail.getRating() != null) {
            instantCoffeeShopRatingTextView.setText(this.getString(R.string.rating_prefix) + shortDetail.getRating() + "");
        }

        // insert into main view
        insertPoint.addView(this.instantInformationView, -1, params);
        // start left to right animation
    }


    /**
     * Set up Url for requesting result from Google Place API Web service
     *
     * @param keyWord            : key word for filter search result
     * @param type               : type of place. It should be "cafe".
     *                           For more detail of type : https://developers.google.com/places/web-service/supported_types
     * @param locationPairString : Device's current location in pair {@link String}. For example (1232,43433)
     * @param radius             : Radius for searching
     * @return
     */
    private String prepareGooglePlaceApiWebServiceRequestUrl(String keyWord, String type,
                                                             String locationPairString,
                                                             int radius) {
        // initial value
        String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=";
        url += keyWord; // append keyword;
        url += "&type=" + type; // append type
        url += "&location=" + locationPairString; // append location information
        url += "&radius=" + radius;
        url += "&key=" + this.getString(R.string.google_place_api_key); // append google api client key

        Log.d(TAG, "Request Google Place API Url : " + url); // log the complete request url for observing
        return url;
    }


    /**
     * Set up Url for requesting result from Google Way points API Web service
     *
     * @param originPairString      : location of origin place in pair string. For example (1232,43433)
     * @param destinationPairString :  location of destination place in pair string. For example (1232,43433)
     * @return : request url
     */
    private String prepareGoogleWayPointsWebServiceRequestUrl(String originPairString
            , String destinationPairString) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=";
        url += originPairString; // append origin
        url += "&destination=" + destinationPairString; // append destination
        url += "&sensor=false";
        url += "&key=" + this.getString(R.string.google_api_key);

        return url;
    }


    /**
     * Read {@link String} content from {@link java.io.InputStream}.
     *
     * @param httpURLConnection : {@link HttpURLConnection} reference to get {@link java.io.InputStream} from.
     * @return : string content for later process such as Json Parsing
     * @throws IOException if error occurs.
     */
    private String readStringFromHttpUrlConnection(HttpURLConnection httpURLConnection)
            throws IOException {
        InputStreamReader isr =
                new InputStreamReader(httpURLConnection.getInputStream());
        BufferedReader br = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append('\n');
        }
        return sb.toString();
    }


}
