package com.coffeeteam.coffeeviet.entities;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class Location {
    private int _id;
    private double latitude;
    private double longitude;

    public Location() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
