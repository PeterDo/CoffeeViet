package com.coffeeteam.coffeeviet.managers.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.coffeeteam.coffeeviet.utils.DeviceNetworkUtil;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;

/**
 * Created by TNS on 4/10/2017.
 * Android apps do take some amount of time to start up, especially on a cold start.
 * There is a delay there that you may not be able to avoid.
 * Instead of leaving a blank screen during this time, why not show the user something nice?
 * This is the approach Google is advocating. Don’t waste the user’s time, but don’t show them a blank,
 * un-configured section of the app the first time they launch it, either.
 * <p>
 * More details for implementing Splash Screen in the right way,
 * read this article : https://www.bignerdranch.com/blog/splash-screens-the-right-way/
 */
public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName(); // tag for logging

    public final static int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1; // location permission request code

    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // make sure device connected to internet
        this.bundle = new Bundle();
        String networkStatus = DeviceNetworkUtil.isNetworkAvailable(this.getApplicationContext()) ?
                DeviceNetworkUtil.STATUS_AVAILABLE
                : DeviceNetworkUtil.STATUS_UNAVAILABLE;
        this.bundle.putString(DeviceNetworkUtil.DEVICE_NETWORK_STATUS, networkStatus);

        // check if device's android version is M or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // make sure location permission was granted
            if (PrefsUtil.getBooleanPref(this.getApplicationContext(),
                    PrefsUtil.LOCATION_PERMISSION_GRANTED)) {
                // Move to to MainActivity for blank screen
                this.moveToMainActivity(this.bundle);
            } else {
                // request device's location permission when device's Android version is higher than M
                /*
                 * Request location permission, so that we can get the location of the
                 * device. The result of the permission request is handled by a callback,
                 * onRequestPermissionsResult.
                 */
                if (ContextCompat.checkSelfPermission(
                        this.getApplicationContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    // save permission grant status
                    PrefsUtil.setBooleanPref(this.getApplicationContext(),
                            PrefsUtil.LOCATION_PERMISSION_GRANTED, true);
                } else {
                    // otherwise, request location permission
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                }
            }
        } else {
            // indicate that , permission was granted
            PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_PERMISSION_GRANTED, true);
            PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.REQUEST_LOCATION_UPDATE, true);
            this.moveToMainActivity(this.bundle);
        }
    }

    /**
     * For remove duplication in code.
     * This method move to {@link MainActivity} including {@link Bundle} containing device's network status.
     *
     * @param bundle : {@link Bundle} to be sent to {@link MainActivity}
     */
    private void moveToMainActivity(Bundle bundle) {
        Intent coffeeShopsIntent = new Intent(this, SignInActivity.class);
        coffeeShopsIntent.putExtra("network_status", bundle);
        this.startActivity(coffeeShopsIntent);
        this.finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult call backs");
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_PERMISSION_GRANTED, true);
                    PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.REQUEST_LOCATION_UPDATE, true);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_PERMISSION_GRANTED, false);
                    PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.REQUEST_LOCATION_UPDATE, false);
                }
                break;
            default:
                break;
        }

        // anyway, move to MainActivity
        this.moveToMainActivity(this.bundle);
    }
}
