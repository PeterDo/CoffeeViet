package com.coffeeteam.coffeeviet.utils;

import android.content.Context;
import android.util.Log;

import com.coffeeteam.coffeeviet.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by TNS on 5/14/2017.
 */

public class DateTimeUtil {

    private static final int IS_NEW_COFFEE_SHOP_DIFF = 14; // in 3 week
    private static final int MAX_INTERVAL_DAYS_FOR_MEANING_STRING = 7; // 1 week
    private static final int MAX_INTERVAL_HOUR_FOR_MEANING_STRING = 2; // 2 hours
    private static final String TAG = "DateTimeUtil";

    public static final String DEFAULT_FORMAT_VALUE = "dd/MM/yyyy HH:mm";

    private DateTimeUtil() {

    }


    /**
     * Detect if this is new coffee shop
     *
     * @param creatingTimeString
     * @return
     */
    public static boolean isNewShop(String creatingTimeString) throws ParseException {
        // make sure parameter is valid
        if (creatingTimeString == null
                || creatingTimeString.isEmpty()) {
            // if no data for creating time, ignore
            return false;
        }
//        Log.d(TAG, creatingTimeString);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date creatingTime = simpleDateFormat.parse(creatingTimeString);
        Date currentTime = new Date();
        long diff = Math.abs(creatingTime.getDate() - currentTime.getDate());
        Log.d(TAG, "Diff result : " + diff);
        return diff <= IS_NEW_COFFEE_SHOP_DIFF;

    }

    /**
     * Convert {@link Date} value to String formatted with specified format.
     *
     * @param format : Format value. For example : dd/MM/yyyy HH:mm
     * @param value  : {@link Date} value to be converted
     * @return : formatted String value;
     */
    public static String toDateTimeFormatString(String format, Date value) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(value);
    }

    /**
     * Display time value with meaning string value.
     * For example : 2h, 15p, 4 ngày ....
     *
     * @param context
     * @param dateTimeString
     * @return meaning string value of specified date in string
     * @throws ParseException
     */
    public static String getDisplayMeaningString(Context context, String dateTimeString) throws ParseException {
        // make sure we have valid value
        if (dateTimeString != null && !dateTimeString.isEmpty()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date value = simpleDateFormat.parse(dateTimeString);
            Date currentTime = new Date();
            // log result
            Log.d(TAG, "diff-time : " + Math.abs(value.getTime() - currentTime.getTime()));
            Log.d(TAG, "diff-date : " + Math.abs(value.getDate() - currentTime.getDate()));

            if ((Math.abs(value.getTime() - currentTime.getTime()) / (Math.pow(10, 3) * 3600))
                    <= MAX_INTERVAL_HOUR_FOR_MEANING_STRING) {
                return (int) (Math.abs(value.getTime() - currentTime.getTime()) / (Math.pow(10, 3) * 3600))
                        + " " + context.getString(R.string.hour_suffix);

            } else {
                if (Math.abs(value.getDate() - currentTime.getDate()) < MAX_INTERVAL_DAYS_FOR_MEANING_STRING) {
                    return Math.abs(value.getDate() - currentTime.getDate()) + " " + context.getString(R.string.day_suffix);
                } else {
                    return dateTimeString;
                }
            }
        } else {
            return dateTimeString;
        }
    }


    /**
     * Get date diff from current date of specified date in String an input parameter
     *
     * @param value
     * @return
     * @throws ParseException
     */
    public static int getDateDiff(String value) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date valueDate = simpleDateFormat.parse(value);
        Date currentTime = new Date();

        return (valueDate.getDate() - currentTime.getDate());
    }
}
