package com.coffeeteam.coffeeviet.entities;

/**
 * Created by TNS on 5/24/2017.
 * Represent information of geometry unit result in web service json request result
 */

public class GooglePlaceWebServiceGeometry {
    private Double latitude;
    private Double longitude;
    private String placeName;
    private String placeId;

    private Double rating;
    private String formattedAddress;
    private String iconUrl;

    public GooglePlaceWebServiceGeometry() {
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "GooglePlaceWebServiceGeometry{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", placeName='" + placeName + '\'' +
                ", placeId='" + placeId + '\'' +
                ", rating=" + rating +
                ", formattedAddress='" + formattedAddress + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                '}';
    }
}
