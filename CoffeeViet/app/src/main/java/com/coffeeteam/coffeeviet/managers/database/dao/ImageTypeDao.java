package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.coffeeteam.coffeeviet.entities.ImageType;
import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.IImageTypeDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.IImageTypeSchema;

import java.util.List;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public class ImageTypeDao extends DbContentProvider implements IImageTypeSchema, IImageTypeDao {

    private Cursor mCursor;
    private List<ImageType> mImageTypeList;
    private ContentValues mInitialValues;

    public ImageTypeDao(SQLiteDatabase db) {
        super(db);
    }


    @Override
    protected <T> T cursorToEntity(Cursor cursor) {
        return null;
    }
}
