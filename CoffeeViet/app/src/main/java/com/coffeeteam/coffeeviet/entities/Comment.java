package com.coffeeteam.coffeeviet.entities;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class Comment {

    private String creator_id;
    private String creating_time;
    private String username;
    private String content;
    private String url_image;
    private String event_id;

    public Comment() {
    }

    public Comment(String creator_id, String username, String content) {
        this.creator_id = creator_id;
        this.username = username;
        this.content = content;
    }

    public String getId() {
        return creator_id;
    }

    public void setId(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getCreating_time() {
        return creating_time;
    }

    public void setCreating_time(String creating_time) {
        this.creating_time = creating_time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
