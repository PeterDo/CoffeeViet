package com.coffeeteam.coffeeviet.entities;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class MenuItem {
    private int _id;
    private String id;
    private String name;
    private int price;
    private String coffee_shop_id;
    private boolean is_active;
    private boolean isFood;
    private String creator_id;

    public MenuItem() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setFood(boolean food) {
        isFood = food;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getCoffee_shop_id() {
        return coffee_shop_id;
    }

    public void setCoffee_shop_id(String coffee_shop_id) {
        this.coffee_shop_id = coffee_shop_id;
    }

    public boolean is_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public boolean isFood() {
        return isFood;
    }

    public void setIsFood(boolean food) {
        isFood = food;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "_id=" + _id +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", coffee_shop_id='" + coffee_shop_id + '\'' +
                ", is_active=" + is_active +
                ", isFood=" + isFood +
                '}';
    }
}
