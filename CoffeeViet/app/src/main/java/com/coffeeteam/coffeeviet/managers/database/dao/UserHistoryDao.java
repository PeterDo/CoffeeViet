package com.coffeeteam.coffeeviet.managers.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.coffeeteam.coffeeviet.entities.UserHistory;
import com.coffeeteam.coffeeviet.managers.database.DbContentProvider;
import com.coffeeteam.coffeeviet.managers.database.dao_interfaces.IUserHistoryDao;
import com.coffeeteam.coffeeviet.managers.database.schema_interface.IUserHistorySchema;

import java.util.List;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public class UserHistoryDao extends DbContentProvider implements IUserHistorySchema, IUserHistoryDao {

    private Cursor mCursor;
    private List<UserHistory> mUserHistoryList;
    private ContentValues mInitialValues;

    public UserHistoryDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    protected <T> T cursorToEntity(Cursor cursor) {
        return null;
    }
}
