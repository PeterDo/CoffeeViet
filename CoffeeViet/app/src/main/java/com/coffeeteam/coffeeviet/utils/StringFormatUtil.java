package com.coffeeteam.coffeeviet.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by TNS on 5/12/2017.
 */

public class StringFormatUtil {

    private static final String TAG = StringFormatUtil.class.getSimpleName();

    private StringFormatUtil() {

    }


    /**
     * @return current time with format : HH:mm
     * NOTE : H vs h is difference between 24 hour vs 12 hour format.
     */
    private static String getOnlyCurrentTime() {
        Date date = new Date(); // get current time
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return simpleDateFormat.format(date);

    }

    /**
     * Compare {@link String} to determine current time is opening time or not
     *
     * @param openingTime
     * @param closingTime
     * @return
     */
    public static boolean isOpeningTime(String openingTime,
                                        String closingTime) throws ParseException {
        if (openingTime.equals(closingTime)) {
            return true;
        }
        String currentTime = getOnlyCurrentTime();
        Date openingTimeDate = new SimpleDateFormat("HH:mm").parse(openingTime);
        Date closeTimeDate = new SimpleDateFormat("HH:mm").parse(closingTime);
        Date currentTimeDate = new SimpleDateFormat("HH:mm").parse(currentTime);
//        Log.d(TAG, "current time " + currentTime);
        // if always opening (24h open)
        return openingTimeDate.before(currentTimeDate) && closeTimeDate.after(currentTimeDate);
    }

}
