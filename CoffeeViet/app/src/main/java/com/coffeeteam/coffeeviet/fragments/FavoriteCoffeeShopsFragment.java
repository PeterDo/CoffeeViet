package com.coffeeteam.coffeeviet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.views.adapters.FavoriteCoffeeShopListCardViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 3/13/2017.
 */

public class FavoriteCoffeeShopsFragment extends Fragment {


    private FavoriteCoffeeShopListCardViewAdapter favoriteCoffeeShopAdapter;
    private RecyclerView recyclerViewCoffeeShops;
    private LinearLayoutManager linearLayoutManager;

    public FavoriteCoffeeShopsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        // set up xml layout for this fragment
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.coffee_shops_fragment, container, false);
        // create sample data
        List<CoffeeShop> sampleList = new ArrayList<>();

        this.favoriteCoffeeShopAdapter = new FavoriteCoffeeShopListCardViewAdapter(this.getContext(), sampleList);

        this.recyclerViewCoffeeShops = (RecyclerView) viewGroup.findViewById(R.id.coffee_shop_recycler_view);
        this.linearLayoutManager = new LinearLayoutManager(this.getActivity());
        this.recyclerViewCoffeeShops.setLayoutManager(this.linearLayoutManager);
        this.recyclerViewCoffeeShops.setHasFixedSize(true);
        this.recyclerViewCoffeeShops.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // super.onScrolled(recyclerView, dx, dy);
                // hide fab when user scroll down the recycler view
                FloatingActionButton fab = (FloatingActionButton) getActivity()
                        .findViewById(R.id.fab); // get fab from Main Activity
                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });

        this.recyclerViewCoffeeShops.setAdapter(this.favoriteCoffeeShopAdapter);
        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
