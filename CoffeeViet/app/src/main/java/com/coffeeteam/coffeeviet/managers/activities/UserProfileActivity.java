package com.coffeeteam.coffeeviet.managers.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeBounds;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.User;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = UserProfileActivity.class.getSimpleName();
    private CircleImageView circleImageViewUserProfilePhoto;
    private ImageView imageViewUserWallpaper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // request transition feature if device's android version is lollipop or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);// set an enter transition
            // set an exit transition
            this.getWindow().setExitTransition(new ChangeBounds());
            this.getWindow().setEnterTransition(new ChangeBounds());

        }
        setContentView(R.layout.activity_user_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.user_profile_toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.circleImageViewUserProfilePhoto = (CircleImageView) this.findViewById(R.id.circleImageView_userProfilePhoto);
        this.imageViewUserWallpaper = (ImageView) this.findViewById(R.id.imageViewUserWallpaper);
        // get AppbarLayout
        AppBarLayout appBarLayout = (AppBarLayout) this.findViewById(R.id.user_profile_app_bar);
        appBarLayout.addOnOffsetChangedListener(this);


        // get intent values and set view content
        User selectedUser = this.getIntent().getParcelableExtra(PrefsUtil.SELECTED_USER);
        // make sure we have valid value
        if (selectedUser != null) {
            this.setupViewContent(selectedUser);
        }
    }

    /**
     * Set content for view
     *
     * @param user : {@link User} information to be displayed
     */
    private void setupViewContent(final User user) {
        TextView textViewUserName = (TextView) this.findViewById(R.id.textView_userProfileName);
        textViewUserName.setText(user.getUsername());
        TextView textViewUserLastLocationString = (TextView) this.findViewById(R.id.textView_userProfileAddress);
        textViewUserLastLocationString.setText(user.getLast_location_string());
        TextView textViewUserEmailAddress = (TextView) this.findViewById(R.id.textView_userProfileEmail);
        textViewUserEmailAddress.setText(user.getEmail());
        // images
        ImageView wallPaperImageView = (ImageView) this.findViewById(R.id.imageViewUserWallpaper);
        Picasso.with(this.getApplicationContext())
                .load(R.drawable.detail_coffeeshop_placeholder)
                .fit()
                .centerCrop()
                .into(wallPaperImageView);

        // get first character
        char prefix = user.getUsername().charAt(0);
        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .width(120)  // width in px
                .height(120) // height in px
                .endConfig()
                .buildRect(String.valueOf(prefix), ColorGenerator.MATERIAL.getRandomColor());

        CircleImageView circleImageViewUserPhoto = (CircleImageView) this.findViewById(R.id.circleImageView_userProfilePhoto);
        circleImageViewUserPhoto.setImageDrawable(textDrawable);
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        Log.d(TAG, "vertical offset -> " + verticalOffset + "");
        // hide the circle image when user collapse the half of appbar layout
        if (Math.abs(verticalOffset) > this.imageViewUserWallpaper.getHeight() / 2) {
            this.circleImageViewUserProfilePhoto.setVisibility(View.INVISIBLE);
        } else {
            this.circleImageViewUserProfilePhoto.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //  To navigate up when the user presses the app icon, you can use the NavUtils class's static method, navigateUpFromSameTask().
            // When you call this method, it finishes the current activity and starts (or resumes) the appropriate parent activity.
            // If the target parent activity is in the task's back stack, it is brought forward
            case android.R.id.home:

                // use back press
                this.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
