package com.coffeeteam.coffeeviet.managers.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeBounds;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.fragments.CoffeeShopsFragment;
import com.coffeeteam.coffeeviet.fragments.FavoriteCoffeeShopsFragment;
import com.coffeeteam.coffeeviet.fragments.FriendListFragment;
import com.coffeeteam.coffeeviet.fragments.InvitationListFragment;
import com.coffeeteam.coffeeviet.managers.database.Database;
import com.coffeeteam.coffeeviet.utils.CustomAnimationUtil;
import com.coffeeteam.coffeeviet.utils.DeviceNetworkUtil;
import com.coffeeteam.coffeeviet.utils.PrefsUtil;
import com.coffeeteam.coffeeviet.views.view_interfaces.OnDataRefreshListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ViewPager.OnPageChangeListener, SwipeRefreshLayout.OnRefreshListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {

    private static final String TAG = MainActivity.class.getSimpleName(); // tag

    private static final int LOCATION_SETTING_REQUEST_CODE = 1;

    //
    private Toolbar toolbar; // tool bar replacing for action bar
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private int selectedFragmentIndex; // save index of current selected fragment
    public static Database mDb;
    //  private DatabaseReference mDatabaseTemp;
    private ArrayList<CoffeeShop> mShopList = new ArrayList<>();
    // fragments
    private Fragment coffeeShopFragment;
    private Fragment favoriteCoffeeShopsFragment;
    private Fragment friendsFragment;
    private Fragment invitationsFragments;
    private Fragment currentSelectedFragment; // flag current selected  fragment

    private SwipeRefreshLayout swipeRefreshLayout;

    // Google API Client for making location feature for application
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest; // for location change handling

    // broadcast receiver
    private BroadcastReceiver connectionStateChangesBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // request transition feature if device's android version is lollipop or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);// set an enter transition
            // set an exit transition
            this.getWindow().setExitTransition(new ChangeBounds());
            this.getWindow().setEnterTransition(new ChangeBounds());
        }
        setContentView(R.layout.activity_main);
        /**==============================
         * Init VIEWs
         * ==============================
         */
        this.toolbar = (Toolbar) findViewById(R.id.homepage_toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        setSupportActionBar(toolbar);
        this.swipeRefreshLayout = (SwipeRefreshLayout) this.findViewById(R.id.primary_swipeRefreshLayout);
        this.swipeRefreshLayout.setColorScheme(
                R.color.colorPrimary
        );
        // initial primary fragments
        this.coffeeShopFragment = new CoffeeShopsFragment();
        this.favoriteCoffeeShopsFragment = new FavoriteCoffeeShopsFragment();
        this.invitationsFragments = new InvitationListFragment();
        this.friendsFragment = new FriendListFragment();
        // by default, current fragment is coffeeShopFragment
        this.currentSelectedFragment = this.coffeeShopFragment;
        // open connection to local DB
        mDb = new Database(this);
        mDb.open();
        Bundle ownerBundle = this.getIntent().getBundleExtra(PrefsUtil.IS_OWNER_BUNDLE);
        Bundle bundle = this.getIntent().getBundleExtra("network_status");
        /////
        this.tabLayout = (TabLayout) this.findViewById(R.id.homepage_tabs);
        this.viewPager = (ViewPager) this.findViewById(R.id.viewpager_homepages);
        this.setUpViewPagers();
        // set up tabs
        this.tabLayout.setupWithViewPager(this.viewPager);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        /**==============================
         * handle EVENTs
         * ==============================
         */
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        // by default, make a refresh
        this.swipeRefreshLayout.setRefreshing(true);
        this.swipeRefreshLayout.setOnRefreshListener(this);

        // get IS_OWNER_BUNDLE from SignInActivity
        if (ownerBundle != null) {
            boolean isOwner = ownerBundle.getBoolean(PrefsUtil.IS_OWNER_KEY);
            if (isOwner) {
                fab.show();
            } else {
                fab.hide();
            }
        }


        // IntentFilter for filtering action connection changes
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.connectionStateChangesBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // if device is not connected to the intent
                // do the offline work for application
                if (!DeviceNetworkUtil.isNetworkAvailable(getApplicationContext())) {
                    // log for observing
                    Log.d(TAG, "Connection is unavailable");
                    // first, stop refreshing
                    if (swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    // show toast offline notification message
                    CustomAnimationUtil.showMessageWithSnackBar(findViewById(R.id.main_coordinate_layout)
                            , getString(R.string.network_unavailable),
                            "#FFEB3B",
                            getString(R.string.retry));
                    // make all fragments knows the state
                    if (currentSelectedFragment != null) {
                        if (currentSelectedFragment instanceof OnDataRefreshListener) {
                            ((OnDataRefreshListener) currentSelectedFragment).doOfflineWork();
                        }
                    }
                } else {
                    Log.d(TAG, "Connection is available");
                    // connection is available, refresh data for the current fragment
                    swipeRefreshLayout.setRefreshing(true);
                    // signals all fragments to fresh data
                    if (currentSelectedFragment != null) {
                        if (currentSelectedFragment instanceof OnDataRefreshListener) {
                            ((OnDataRefreshListener) currentSelectedFragment).refreshFirebaseData();
                        }
                    }
                }
            }
        };
        // register receiver
        this.registerReceiver(this.connectionStateChangesBroadcastReceiver, intentFilter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // move to see nearby coffee shops on custom Google Map
                Intent nearbyCoffeeShopsGoogleMapsIntent = new Intent(MainActivity.this, NearbyCoffeeShopsActivity.class);
                // add intent parameters
                nearbyCoffeeShopsGoogleMapsIntent.putExtra(NearbyCoffeeShopsActivity.MAP_ACTION_TYPE,
                        NearbyCoffeeShopsActivity.ACTION_LOAD_NEAR_BY_COFFEE_SHOP // tell activity to load all recommendations and nearby shops
                );
                startActivity(nearbyCoffeeShopsGoogleMapsIntent);
            }
        });

        navigationView.setNavigationItemSelectedListener(this);

        // build Google API Client
        this.buildGoogleApiClientApi();

        if (PrefsUtil.getBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_SETTING_REQUEST_SATISFIED)) {
            // create location request
            this.locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(5 * 1000); // 5 second, in milliseconds for fast interval request
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        this.viewPager.setCurrentItem(this.selectedFragmentIndex);
        if (this.googleApiClient.isConnected()
                && PrefsUtil.getBooleanPref(this.getApplicationContext(),
                PrefsUtil.REQUEST_LOCATION_UPDATE)) {
            this.startLocationUpdate();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //  this.viewPager.setCurrentItem(this.selectedFragmentIndex);
    }


    @Override
    protected void onPause() {
        super.onPause();
        this.stopLocationUpdate();
    }

    /**
     * Callback method when other activities finish
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_SETTING_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    // All required changes were successfully made
                    PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_SETTING_REQUEST_SATISFIED, true);
                    this.startLocationUpdate();
                } else if (resultCode == RESULT_CANCELED) {
                    // The user was asked to change settings, but chose not to
                    PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_SETTING_REQUEST_SATISFIED, false);

                }
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings: {
                return true;

            }

            case R.id.action_signout: {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, SignInActivity.class));
                finish();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Initialize fragments for view pagers via {@link ViewPagerAdapter}.
     * Then view pager will be set up into View Pager Adapter
     */
    private void setUpViewPagers() {
        this.viewPagerAdapter = new ViewPagerAdapter(this.getSupportFragmentManager());
        // start to add fragments into view pagers
        this.viewPagerAdapter.addFragment(this.coffeeShopFragment,
                this.getString(R.string.coffee_shops_tab_title)); // coffee shop fragment
        this.viewPagerAdapter.addFragment(this.favoriteCoffeeShopsFragment,
                this.getString(R.string.favorite_shops_tab_title)); // favorite coffee shop fragment
        this.viewPagerAdapter.addFragment(this.friendsFragment,
                this.getString(R.string.friends_tab_title)); // friend list fragment
        this.viewPagerAdapter.addFragment(this.invitationsFragments,
                this.getString(R.string.invitation_tab_title)); // invitation fragment
        // set up adapter for view pager
        if (this.viewPager != null) {
            this.viewPager.setAdapter(this.viewPagerAdapter);
            this.viewPager.setOnPageChangeListener(this);
        }

        // set default selected fragment index to 0
        this.currentSelectedFragment = this.viewPagerAdapter.getItem(0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        this.selectedFragmentIndex = position;
        this.currentSelectedFragment = this.viewPagerAdapter.getItem(position);

        // get current fragment
        // save selected position into preference
        /// PrefsUtil.savePref(this, PrefsUtil.LAST_SELECTED_FRAGMENT_INDEX_PREFENCE_KEY, position + "");
        // get current fragment

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onRefresh() {
        // signals all fragments to fresh data
        if (this.currentSelectedFragment != null) {
            if (this.currentSelectedFragment instanceof OnDataRefreshListener) {
                ((OnDataRefreshListener) this.currentSelectedFragment).refreshFirebaseData();
            } else {
                if (this.swipeRefreshLayout.isRefreshing()) {
                    this.swipeRefreshLayout.setRefreshing(false);
                }
            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {


        //  Once you have connected to Google Play services and the location services API, you can get the current location settings of a user's device.
        //  To do this, create a LocationSettingsRequest.Builder, and add one or more location requests.
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(this.locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> pendingResult = LocationServices.SettingsApi.checkLocationSettings(this.googleApiClient, builder.build());
        pendingResult.setResultCallback(this);


        // GoogleApiClient connected , start to get last device's location
        // first, make sure we had access location permission from user
        if (PrefsUtil.getBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_PERMISSION_GRANTED)) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(this.googleApiClient);

            // make sure last location is valid
            if (lastLocation != null) {

                Log.d(TAG, "Device's current location : ( " + lastLocation.getLatitude() + "," + lastLocation.getLongitude() + ")");

                // save this location into application preference
                PrefsUtil.setStringPref(this.getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LATITUDE,
                        lastLocation.getLatitude() + "");
                PrefsUtil.setStringPref(this.getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LONGITUDE,
                        lastLocation.getLongitude() + "");

                // start listen to location change
                if (PrefsUtil.getBooleanPref(this.getApplicationContext(), PrefsUtil.REQUEST_LOCATION_UPDATE)) {
                    this.startLocationUpdate();
                }
            } else {
                // last location is invalid, log for observing
                Log.e(TAG, "onConnected, last location is null");
            }

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google Client Api connected failed");
    }


    /**
     * Google Location Api Support Location feature for building location awareness application
     * Build the Play services client for use by the Fused Location Provider and the Places API.
     * Use the addApi() method to request the Google Places API and the Fused Location Provider.
     */
    private void buildGoogleApiClientApi() {
        if (this.googleApiClient == null) {
            this.googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */,
                            this /* OnConnectionFailedListener */)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
        }
        this.googleApiClient.connect();
    }

    /**
     * Make a request to {@link LocationServices} to listen for changes of device's location
     */
    private void startLocationUpdate() {

        if (this.locationRequest == null) {
            // create location request
            this.locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(1000); // 1 second, in milliseconds
        }
        if (this.googleApiClient != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.googleApiClient
                    , this.locationRequest
                    , this);
        }
    }

    /**
     * Stop to listen location changes
     */
    private void stopLocationUpdate() {
        LocationServices.FusedLocationApi.removeLocationUpdates(this.googleApiClient, this);
    }


    /**
     * Handle when device's location changed
     *
     * @param location : Updated location
     */
    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Device's location changed : ( " + location.getLatitude() + "," + location.getLongitude() + " )");
        // save last location
        PrefsUtil.setStringPref(this.getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LATITUDE, location.getLatitude() + "");
        PrefsUtil.setStringPref(this.getApplicationContext(), PrefsUtil.DEVICE_CURRENT_LOCATION_LONGITUDE, location.getLongitude() + "");


    }


    /**
     * When the PendingResult returns, the client can check the location settings by looking at the status code from the LocationSettingsResult object.
     *
     * @param locationSettingsResult
     */
    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        final LocationSettingsStates locationSettingsStates = locationSettingsResult.getLocationSettingsStates();
        // analyze status code
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied.
                // The client can initialize location requests here.
                PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_SETTING_REQUEST_SATISFIED, true);
                this.startLocationUpdate();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location setting are not satisfied.
                // Show the setting dialog.
                try {
                    status.startResolutionForResult(this, LOCATION_SETTING_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                    // user ignored location settings
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix t
                // settings so we won't show the dialog.
                PrefsUtil.setBooleanPref(this.getApplicationContext(), PrefsUtil.LOCATION_SETTING_REQUEST_SATISFIED, true);
                break;
        }
    }


    /**
     * Prepare fragments for {@link MainActivity} View
     * {@link ViewPagerAdapter} being as adapter for view pager
     */
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> listOfFragments = new ArrayList<>();
        private final List<String> listOfTitles = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        /**
         * Get {@link android.app.Fragment} by index
         *
         * @param position : index of element in list
         * @return
         */
        @Override
        public Fragment getItem(int position) {
            return this.listOfFragments.get(position);
        }

        @Override
        public int getCount() {
            return this.listOfFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.listOfTitles.get(position);
        }

        /**
         * Add new Fragment Item into list of fragments.
         *
         * @param fragment : {@link android.app.Fragment}
         * @param title
         */
        public void addFragment(Fragment fragment, String title) {
            this.listOfFragments.add(fragment);
            this.listOfTitles.add(title);
        }

    }

    @Override
    protected void onDestroy() {
        mDb.close();
        super.onDestroy();
        // disconnect GoogleApiClient
        if (this.googleApiClient != null) {
            this.googleApiClient.disconnect();
        }
        // unregister broadcast receiver
        if (this.connectionStateChangesBroadcastReceiver != null) {
            this.unregisterReceiver(this.connectionStateChangesBroadcastReceiver);
        }
    }

}
