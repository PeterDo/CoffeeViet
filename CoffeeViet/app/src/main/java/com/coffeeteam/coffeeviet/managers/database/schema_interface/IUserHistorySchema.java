package com.coffeeteam.coffeeviet.managers.database.schema_interface;

/**
 * Created by Hau-Do on 22/03/2017.
 */

public interface IUserHistorySchema {
    /**
     * private int _id;
     private String id;
     private String user_id;
     private String coffee_shop_id;
     private String even_id;
     private String history_time;
     private String description;
     private boolean is_active;
     */
    public static final String USER_HISTORY_TABLE = "USER_HISTORY";

    public static final String ID = "_id";
    public static final String USER_HISTORY_ID = "id";
    public static final String USER_HISTORY_USER_ID = "user_id";
    public static final String USER_HISTORY_COFFEE_SHOP_ID = "coffee_shop_id";
    public static final String USER_HISTORY_EVENT_ID = "even_id";
    public static final String USER_HISTORY_HISTORY_TIME = "history_time";
    public static final String USER_HISTORY_DESCRIPTION = "description";
    public static final String USER_HISTORY_ACTIVE = "active";

    public static final String USER_HISTORY_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + USER_HISTORY_TABLE
            + " ("
            + ID
            + " INTEGER PRIMARY KEY, "
            + USER_HISTORY_ID
            + " TEXT NOT NULL, "
            + USER_HISTORY_USER_ID
            + " TEXT NOT NULL"
            + USER_HISTORY_COFFEE_SHOP_ID
            + " TEXT NOT NULL,"
            + USER_HISTORY_EVENT_ID
            + " TEXT NOT NULL, "
            + USER_HISTORY_HISTORY_TIME
            + " TEXT"
            + USER_HISTORY_DESCRIPTION
            + " TEXT"
            + USER_HISTORY_ACTIVE
            + " BOOLEAN"
            + ")";

    public static final String[] USER_HISTORY_COLUMNS = new String[]{
            ID, USER_HISTORY_ID, USER_HISTORY_USER_ID, USER_HISTORY_COFFEE_SHOP_ID,
            USER_HISTORY_EVENT_ID, USER_HISTORY_HISTORY_TIME, USER_HISTORY_DESCRIPTION, USER_HISTORY_ACTIVE};
}
