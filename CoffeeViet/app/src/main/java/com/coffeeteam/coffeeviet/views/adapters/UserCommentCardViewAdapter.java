package com.coffeeteam.coffeeviet.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.Comment;
import com.coffeeteam.coffeeviet.utils.DateTimeUtil;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 3/29/2017.
 * Custom Adapter for {@link RecyclerView} of User comments fragment
 */

/**
 * Overrided by Hau-Do on 5/23/2017
 */

public class UserCommentCardViewAdapter extends
        RecyclerView.Adapter<UserCommentCardViewAdapter.ViewHolder> {

    private static final String TAG = UserCommentCardViewAdapter.class.getSimpleName();

    private Context mContext;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;

    private List<String> mCommentIds = new ArrayList<>();
    private List<Comment> mComments = new ArrayList<>();


    public UserCommentCardViewAdapter(Context context, DatabaseReference ref) {
        mContext = context;
        mDatabaseReference = ref;

        // Create child event listener
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

                // A new comment has been added, then add it to the displayed list
                Comment comment = dataSnapshot.getValue(Comment.class);

                // Update RecyclerView
                mCommentIds.add(dataSnapshot.getKey());
                mComments.add(comment);
                notifyItemInserted(mComments.size() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so displayed the changed comment.
                Comment newComment = dataSnapshot.getValue(Comment.class);
                String commentKey = dataSnapshot.getKey();

                int commentIndex = mCommentIds.indexOf(commentKey);
                if (commentIndex > -1) {
                    // Replace with the new data
                    mComments.set(commentIndex, newComment);

                    // Update the RecyclerView
                    notifyItemChanged(commentIndex);
                } else {
                    Log.w(TAG, "onChildChanged:unknown_child:" + commentKey);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                // A comment has changed, use the key to determine if we are displaying this
                // comment and if so remove it.
                String commentKey = dataSnapshot.getKey();

                int commentIndex = mCommentIds.indexOf(commentKey);
                if (commentIndex > -1) {
                    // Remove data from the list
                    mCommentIds.remove(commentIndex);
                    mComments.remove(commentIndex);

                    // Update the RecyclerView
                    notifyItemRemoved(commentIndex);
                } else {
                    Log.w(TAG, "onChildRemoved:unknown_child:" + commentKey);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());

                // A comment has changed position, use the key to determine if we are
                // displaying this comment and if so move it.
                Comment movedComment = dataSnapshot.getValue(Comment.class);
                String commentKey = dataSnapshot.getKey();

                /**
                 * ...
                 */
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                Toast.makeText(mContext, "Failed to load comments.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        ref.addChildEventListener(childEventListener);

        // Store reference to listener so it can be removed on app stop
        mChildEventListener = childEventListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coffee_shop_comment_list_item, parent, false);

        return new UserCommentCardViewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comment comment = mComments.get(position);
        // make sure comment content is not empty
        if (!comment.getContent().isEmpty() && !comment.getUsername().isEmpty()) {
            holder.textViewUserName.setText(comment.getUsername());
            holder.textViewUserComment.setText(comment.getContent());
            try {
                holder.textViewCommentTime.setText(
                        DateTimeUtil.getDisplayMeaningString(this.mContext, comment.getCreating_time()));
            } catch (ParseException e) {
                holder.textViewCommentTime.setText(comment.getCreating_time());
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    /**
     * Provide a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewUserName;
        public TextView textViewUserComment;
        public TextView textViewCommentTime;
        public ImageButton imageButtonContextMenu;

        public ViewHolder(View view) {
            super(view);
            // start to bind view by id
            this.textViewUserName = (TextView) view.findViewById(R.id.textview_user_comment_name);
            this.textViewUserComment = (TextView) view.findViewById(R.id.textview_user_comment);
            this.textViewCommentTime = (TextView) view.findViewById(R.id.textview_user_comment_time);
            this.imageButtonContextMenu = (ImageButton) view.findViewById(R.id.imagebutton_user_comment_more_menu);


        }
    }

}
