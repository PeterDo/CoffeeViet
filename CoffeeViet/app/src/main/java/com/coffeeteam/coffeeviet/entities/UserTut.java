package com.coffeeteam.coffeeviet.entities;

import java.util.Date;

import static android.R.attr.id;

/**
 * Created by Hau-Do on 15/03/2017.
 */

public class UserTut {
    private int _id;
    private int userId;
    private String username;
    private String email;
    private Date createdDate;

    public UserTut() {
    }


    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
