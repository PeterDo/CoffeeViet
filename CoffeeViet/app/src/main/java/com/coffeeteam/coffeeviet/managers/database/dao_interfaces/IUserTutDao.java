package com.coffeeteam.coffeeviet.managers.database.dao_interfaces;

import com.coffeeteam.coffeeviet.entities.UserTut;

import java.util.List;

/**
 * Created by Hau-Do on 15/03/2017.
 */

/**
 * declare basic functions of the USER data-access layer
 */
public interface IUserTutDao {
    public UserTut fetchUserById(int userId);

    public List<UserTut> fetchAllUsers();

    public boolean addUser(UserTut user);

    public boolean addUser(List<UserTut> userTuts);

    public boolean deleteAllUsers();
}
