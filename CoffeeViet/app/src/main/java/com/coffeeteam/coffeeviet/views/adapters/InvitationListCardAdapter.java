package com.coffeeteam.coffeeviet.views.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.utils.CardViewAnimationUtil;
import com.coffeeteam.coffeeviet.views.view_interfaces.ViewHolderFieldAccess;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by TNS on 3/18/2017.
 */

public class InvitationListCardAdapter extends
        RecyclerView.Adapter<InvitationListCardAdapter.ViewHolder> implements View.OnClickListener {
    private Context context; // context of adapter
    private List<Object> listOfInvitations;

    public InvitationListCardAdapter(Context context, List<Object> listOfInvitation) {
        this.context = context;
        this.listOfInvitations = listOfInvitation;
    }

    /**
     * Bind view layout from resource
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public InvitationListCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invitation_list_item, parent, false);

        return new InvitationListCardAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InvitationListCardAdapter.ViewHolder holder, int position) {
        // get coffee shop at index is position as second parameter
        // User currentUser = this.listOfInvitations.get(position);
        // start to set up view values for ViewHolder
        holder.circleImageViewActiveFriendProfilePhoto.setImageResource(R.drawable.sample_user_image);
        holder.textViewInvitionHeader.setText(this.context.getString(R.string.sample_invitation_header));
        holder.textViewInvitaionMessage.setText(this.context.getString(R.string.sample_inviation_message));
        holder.textViewInvitationStatus.setText(this.context.getString(R.string.invitation_accepted));


        // TEST VIEW VISIBILITY
        Random random = new Random();
        if (random.nextInt() % 2 == 0) {
            holder.relativeLayoutButtonsContainer.setVisibility(View.VISIBLE);
            holder.textViewInvitationStatus.setVisibility(View.INVISIBLE);
            if (random.nextInt() % 2 == 0) {
                holder.textViewInvitationStatus.setText(this.context.getString(R.string.invitation_accepted));

            } else {
                holder.textViewInvitationStatus.setText(this.context.getString(R.string.invitation_decline));
            }
        } else {
            holder.relativeLayoutButtonsContainer.setVisibility(View.GONE);
            holder.textViewInvitationStatus.setVisibility(View.VISIBLE);
        }

        if (random.nextInt() % 2 == 0) {
            holder.getTextViewInvitionTime.setText(this.context.getString(R.string.sample_last_active_time_1));
        } else {
            holder.getTextViewInvitionTime.setText(this.context.getString(R.string.sample_last_active_time_2));
        }

        // card view on touch animation
        CardViewAnimationUtil.flipOnTouchCardAnimation(this.context, holder);
        // start animation
        CardViewAnimationUtil.animateCardViewAppearance(this.context, holder, position);
    }

    @Override
    public int getItemCount() {
        return this.listOfInvitations.size();
    }

    /**
     * Handle when user click buttons
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.button_accept_invitation) {

        } else {

        }
        // anyway, make the buttons container gone

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements ViewHolderFieldAccess {
        public CardView cardView;
        // each data item will contain coffee shop information
        public CircleImageView circleImageViewActiveFriendProfilePhoto;
        public TextView textViewInvitionHeader;
        public TextView textViewInvitaionMessage;
        public TextView textViewInvitationStatus;
        public TextView getTextViewInvitionTime;
        public RelativeLayout relativeLayoutButtonsContainer;
        // buttons
        public ImageButton buttonAcceptInviation;
        public ImageButton buttonDeclineInviation;

        public boolean shouldAnimated;

        // member constructor with parameter is view containing all child elements
        public ViewHolder(View view) {
            super(view);
            this.cardView = (CardView) view.findViewById(R.id.cardview_invitation_item);
            // start to bind view
            this.circleImageViewActiveFriendProfilePhoto = (CircleImageView)
                    view.findViewById(R.id.circle_imageview_invitation_friend_photo);
            this.textViewInvitionHeader = (TextView) view.findViewById(R.id.textview_invitation_header);
            this.textViewInvitaionMessage = (TextView) view.findViewById(R.id.textview_invitation_mesage);
            this.textViewInvitationStatus = (TextView) view.findViewById(R.id.textview_invitation_status);
            this.getTextViewInvitionTime = (TextView) view.findViewById(R.id.textview_invitation_time);
            this.buttonAcceptInviation = (ImageButton) view.findViewById(R.id.button_accept_invitation);
            this.buttonAcceptInviation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    relativeLayoutButtonsContainer.setVisibility(View.GONE);
                    textViewInvitationStatus.setVisibility(View.VISIBLE);
                }
            });
            this.buttonDeclineInviation = (ImageButton) view.findViewById(R.id.button_decline_invitation);
            this.buttonDeclineInviation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    relativeLayoutButtonsContainer.setVisibility(View.GONE);
                    textViewInvitationStatus.setVisibility(View.VISIBLE);
                }
            });
            this.relativeLayoutButtonsContainer = (RelativeLayout) view.findViewById(R.id.buttons_container);
            this.shouldAnimated = true;
        }

        @Override
        public CardView getCardView() {
            return this.cardView;
        }

        @Override
        public boolean getShouldAnimateValue() {
            return this.shouldAnimated;
        }

        @Override
        public void shouldAnimate(boolean value) {
            this.shouldAnimated = value;
        }
    }

}
