package com.coffeeteam.coffeeviet.entities;

/**
 * Created by Hau-Do on 13/03/2017.
 */

public class FriendRelationship {
    private int _id;
    private String user_id;
    private String friend_id;
    private String relationship_time;


    public FriendRelationship() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFriend_id() {
        return friend_id;
    }

    public void setFriend_id(String friend_id) {
        this.friend_id = friend_id;
    }

    public String getRelationship_time() {
        return relationship_time;
    }

    public void setRelationship_time(String relationship_time) {
        this.relationship_time = relationship_time;
    }
}
