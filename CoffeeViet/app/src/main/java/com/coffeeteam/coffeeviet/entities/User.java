package com.coffeeteam.coffeeviet.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hau-Do on 17/03/2017.
 */

public class User implements Parcelable {
    private String id;
    private String name;
    private String username;
    private String password;
    private String email;
    private String phone_number;
    private Location lasted_acive_time;
    private String own_coffee_shop_id;
    private String last_location_string;
    private boolean is_online;
    private boolean is_acive;
    private boolean is_owner;

    public User() {
    }

    public User(String username, String email, boolean is_owner) {
        this.username = username;
        this.email = email;
        this.is_owner = is_owner;
    }

    protected User(Parcel in) {
        id = in.readString();
        name = in.readString();
        username = in.readString();
        password = in.readString();
        email = in.readString();
        phone_number = in.readString();
        own_coffee_shop_id = in.readString();
        last_location_string = in.readString();
        is_online = in.readByte() != 0;
        is_acive = in.readByte() != 0;
        is_owner = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getLast_location_string() {
        return last_location_string;
    }

    public void setLast_location_string(String last_location_string) {
        this.last_location_string = last_location_string;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Location getLasted_acive_time() {
        return lasted_acive_time;
    }

    public void setLasted_acive_time(Location lasted_acive_time) {
        this.lasted_acive_time = lasted_acive_time;
    }

    public String getOwn_coffee_shop_id() {
        return own_coffee_shop_id;
    }

    public void setOwn_coffee_shop_id(String own_coffee_shop_id) {
        this.own_coffee_shop_id = own_coffee_shop_id;
    }

    public boolean is_online() {
        return is_online;
    }

    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }

    public boolean is_acive() {
        return is_acive;
    }

    public void setIs_acive(boolean is_acive) {
        this.is_acive = is_acive;
    }

    public boolean is_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(username);
        parcel.writeString(password);
        parcel.writeString(email);
        parcel.writeString(phone_number);
        parcel.writeString(own_coffee_shop_id);
        parcel.writeString(last_location_string);
        parcel.writeByte((byte) (is_online ? 1 : 0));
        parcel.writeByte((byte) (is_acive ? 1 : 0));
        parcel.writeByte((byte) (is_owner ? 1 : 0));
    }
}
