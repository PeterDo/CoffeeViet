package com.coffeeteam.coffeeviet.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

/**
 * Created by TNS on 5/2/2017.
 * Helper class for working with application {@link android.content.SharedPreferences}
 */

public class PrefsUtil {
    public static final String TAG = PrefsUtil.class.getSimpleName(); // tag as preference file key
    public static final String PREF_DEFAULT_VALUE = "pref_default_value";
    public static final String LAST_SELECTED_FRAGMENT_INDEX_PREFENCE_KEY = "last_selected_fragment_index_prefence_key";

    // intents parameter keys from MainActivity
    public static final String CURRENT_SHOP = "CURRENT_SHOP";
    public static final String SELECTED_EVENT = "selected_event";
    public static final String SELECTED_USER = "selected_user";

    public final static String IS_OWNER_KEY = "IS_OWNER_KEY";
    public final static String IS_OWNER_BUNDLE = "IS_OWNER_BUNDLE";


    // Related location Preference keys
    public final static String LOCATION_PERMISSION_GRANTED = "location_permission_granted";
    public final static String DEVICE_CURRENT_LOCATION_LATITUDE = "device_current_location_latitude";
    public final static String DEVICE_CURRENT_LOCATION_LONGITUDE = "device_current_location_longitude";
    public final static String REQUEST_LOCATION_UPDATE = "request_location_update";
    public final static String LOCATION_SETTING_REQUEST_SATISFIED = "location_setting_request_satisfied";

    /**
     * private constructor for non-instantiability
     */
    private PrefsUtil() {

    }

    /**
     * @param context
     * @param prefKey
     * @param value
     */
    public static void setStringPref(Context context, String prefKey, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        // start to save preference value
        editor.putString(prefKey, value);
        editor.commit();
    }

    public static String getStringPrefValue(Context context, String prefKey) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return sharedPreferences.getString(prefKey, PREF_DEFAULT_VALUE);
    }


    /**
     * Get all preferences with boolean values
     *
     * @param context : {@link Context} using this method
     * @param prefKey : key of desired preference
     * @return
     */
    public static boolean getBooleanPref(Context context, String prefKey) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(prefKey, false); // default value to false
    }

    /**
     * Set value for specified preference with a concrete key in {@link String}
     *
     * @param context : {@link Context} using this method
     * @param prefKey : key of desired preference
     * @param value   : value to be set
     */
    public static void setBooleanPref(Context context, String prefKey, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        // start to save preference value
        editor.putBoolean(prefKey, value);
        editor.commit();
    }


}
