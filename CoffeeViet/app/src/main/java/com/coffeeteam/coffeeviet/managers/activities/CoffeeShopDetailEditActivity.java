package com.coffeeteam.coffeeviet.managers.activities;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.CoffeeShop;
import com.coffeeteam.coffeeviet.entities.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CoffeeShopDetailEditActivity extends BaseActivity {

    private static final String TAG = "CoffeeShopDetailEditActivity";
    private static final String REQUIRED = "Required";

    private DatabaseReference mDatabase;

    private EditText mShopNameField;
    private EditText mShopAddressField;
    private EditText mOpeningTimeField;
    private EditText mClosingTimeField;
    private Button mDone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee_shop_detail_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(""); // empty title
        setSupportActionBar(toolbar);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        // load content for top ImageView
        ImageView localBarThemeImageView = (ImageView) this.findViewById(R.id.new_shop_theme_imageview);
        Picasso.with(this)
                .load(R.drawable.local_bar_theme)
                .fit()
                .into(localBarThemeImageView);
        // register text editor click listener.
        this.mShopNameField = (EditText) findViewById(R.id.input_coffee_shop_name);
        this.mShopAddressField = (EditText) findViewById(R.id.input_coffee_shop_address);
        this.mOpeningTimeField = (EditText) this.findViewById(R.id.input_coffee_shop_open_time);
        this.mClosingTimeField = (EditText) this.findViewById(R.id.input_coffee_shop_closing_time);
        this.mDone = (Button) findViewById(R.id.button_done);

        /**
         * EVENTs
         */
        this.mOpeningTimeField.setOnClickListener(new View.OnClickListener() {
            // show TimePickerDialog for user to select
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                // showTimePickerDialog(openTimeEditText);
                showTimePickerDialog(mOpeningTimeField);
            }
        });

        this.mClosingTimeField.setOnClickListener(new View.OnClickListener() {
            // show TimePickerDialog for user to select
            @Override
            public void onClick(View view) {
                //showTimePickerDialog(closingTimeEditText);
                showTimePickerDialog(mClosingTimeField);
            }
        });

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitShop();
            }
        });
    }

    private void submitShop() {
        final String name = mShopNameField.getText().toString();
        final String address = mShopAddressField.getText().toString();
        final String opening_time = mOpeningTimeField.getText().toString();
        final String closing_time = mClosingTimeField.getText().toString();

        if (TextUtils.isEmpty(name)) {
            mShopNameField.setError(REQUIRED);
            return;
        }

        if (TextUtils.isEmpty(address)) {
            mShopAddressField.setError(REQUIRED);
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);
        showProgressDialog();

        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);

                        if (user == null) {
                            // User is null, error out
                            hideProgressDialog();
                            Toast.makeText(CoffeeShopDetailEditActivity.this,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {

                            writeNewCoffeeShop(userId, name, address, opening_time, closing_time);
                        }

                        // Finish this Activity, back to the stream
                        setEditingEnabled(true);
                        finish();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        setEditingEnabled(true);
                        hideProgressDialog();
                    }
                });
    }

    private void writeNewCoffeeShop(String userId, String name, String address, String opening_time, String closing_time) {
        String key = mDatabase.child("posts").push().getKey();
        CoffeeShop coffeeShop = new CoffeeShop(userId, name, address, opening_time, closing_time);
        Map<String, Object> shopValues = coffeeShop.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/shops/" + key, shopValues); // for 'shops' Node
        childUpdates.put("/user-shops/" + userId + "/" + key, shopValues); // for 'user-shops' Node

        mDatabase.updateChildren(childUpdates);
    }

    private void setEditingEnabled(boolean enabled) {
        mShopNameField.setEnabled(enabled);
        mShopAddressField.setEnabled(enabled);
        if (enabled) {
            mDone.setVisibility(View.VISIBLE);
        } else {
            mDone.setVisibility(View.GONE);
        }
    }

    /**
     * @param editText
     */
    private void showTimePickerDialog(final EditText editText) {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(CoffeeShopDetailEditActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        editText.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true); //Yes 24 hour time
        mTimePicker.show();
    }


}
