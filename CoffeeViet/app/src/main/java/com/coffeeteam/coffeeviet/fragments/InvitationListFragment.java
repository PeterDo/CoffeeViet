package com.coffeeteam.coffeeviet.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.views.adapters.InvitationListCardAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 3/13/2017.
 * To be updated later.
 */

public class InvitationListFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerViewInvitations;
    private RecyclerView.Adapter invitationsAdapter;

    public InvitationListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        // set up xml layout for this fragment
        // set up xml layout for this fragment
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.invitation_list_fragment, container, false);
        // create sample data
        List<Object> sampleList = new ArrayList<>();
        sampleList.add(new Object());
        sampleList.add(new Object());
        sampleList.add(new Object());
        sampleList.add(new Object());
        sampleList.add(new Object());
        sampleList.add(new Object());
        sampleList.add(new Object());
        sampleList.add(new Object());

        this.invitationsAdapter = new InvitationListCardAdapter(this.getContext(), sampleList);

        this.recyclerViewInvitations = (RecyclerView) viewGroup.findViewById(R.id.invitation_list_recycler_view);
        this.linearLayoutManager = new LinearLayoutManager(this.getActivity());
        this.recyclerViewInvitations.setLayoutManager(this.linearLayoutManager);
        this.recyclerViewInvitations.setHasFixedSize(true);
        this.recyclerViewInvitations.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // super.onScrolled(recyclerView, dx, dy);
                // hide fab when user scroll down the recycler view
                FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab); // get fab from Main Activity
                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });

        this.recyclerViewInvitations.setAdapter(this.invitationsAdapter);
        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


}
