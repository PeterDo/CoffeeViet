package com.coffeeteam.coffeeviet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.coffeeteam.coffeeviet.R;
import com.coffeeteam.coffeeviet.entities.MenuItem;
import com.coffeeteam.coffeeviet.views.adapters.MenuFoodItemListViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 3/24/2017.
 */

public class MenuFoodItemListFragment extends Fragment {

    public static final String TAG = MenuFoodItemListFragment.class.getSimpleName();

    private ListView listViewMenuItems;
    private MenuFoodItemListViewAdapter menuItemListViewAdapter;
    private DatabaseReference databaseReference;
    private Query query;
    private String firebaseKey;


    public MenuFoodItemListFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container
            , Bundle savedInstanceState) {
        // set up xml layout for this fragment
        final ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.coffee_shop_menu_fragment, container, false);
        // get bundle argument from parent Activity
        Bundle bundle = this.getArguments();
        this.firebaseKey = bundle.getString("coffeeShop_key");
        Log.d(TAG, "Firebase key : " + firebaseKey);
        // create sample data
        final List<MenuItem> menuItems = new ArrayList<>();
        // start to fetch firebase data
        if (!this.firebaseKey.isEmpty()) {
            // get shop foods
            this.databaseReference = FirebaseDatabase.getInstance().getReference();
            this.query = this.databaseReference.child("shop-foods").child(this.firebaseKey);
            this.query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // remove all items
                    if (menuItems != null) {
                        menuItems.clear();
                    }

                    for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                        //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                        MenuItem item = shopDataSnapshot.getValue(MenuItem.class);
                        item.setIsFood(true); // this is food, not drink
                        menuItems.add(item);
                        Log.d(TAG, "Menu item information : " + item.toString());
                        // Toast.makeText(getApplication(), "shop: " + shop.getName(), Toast.LENGTH_SHORT).show();
                    }
                    prepareContent(viewGroup, menuItems);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // log error
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        }


        return viewGroup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * Utility method for preparing view content.
     * This method will be used inside addValueEventListener method of {@link Query} object
     *
     * @param viewGroup       : {@link ViewGroup} from onCreateView method
     * @param listOfMenuItems : List of {@link MenuItem} will be displayed
     */
    private void prepareContent(ViewGroup viewGroup, List<MenuItem> listOfMenuItems) {
        this.listViewMenuItems = (ListView) viewGroup.findViewById(R.id.listview_menu_items);
        this.menuItemListViewAdapter = new MenuFoodItemListViewAdapter(this.getActivity(),
                R.layout.coffee_shop_menu_fragment,
                listOfMenuItems);
        // make sure list view can be scrolled inside scroll nested view
        ViewCompat.setNestedScrollingEnabled(this.listViewMenuItems, true);
        // set adapter for list view
        this.listViewMenuItems.setAdapter(this.menuItemListViewAdapter);
        this.listViewMenuItems.setEmptyView(viewGroup.findViewById(R.id.textview_emptylist_placeholder));
        this.listViewMenuItems.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                AHBottomNavigation ahBottomNavigation = (AHBottomNavigation) getActivity().
                        findViewById(R.id.coffee_shop_detail_bottom_navigation);
                // scroll down
                if (lastFirstVisibleItem < i) {
                    ahBottomNavigation.setVisibility(View.INVISIBLE);
                } else if (lastFirstVisibleItem > i) { // scroll up
                    ahBottomNavigation.setVisibility(View.VISIBLE);
                }
                lastFirstVisibleItem = i;
            }
        });
    }
}
