package com.coffeeteam.coffeeviet;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.coffeeteam.coffeeviet.entities.Comment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TNS on 5/17/2017.
 */

@RunWith(AndroidJUnit4.class)
public class FirebaseDataUnitTest {

    @Test
    public void testLoadListOfUserComments() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("shop-comments").child("shop1");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Comment> listOfComments = new ArrayList<>();
                for (DataSnapshot shopDataSnapshot : dataSnapshot.getChildren()) {
                    //  Toast.makeText(getApplication(), "2", Toast.LENGTH_SHORT).show();
                    Comment comment = shopDataSnapshot.getValue(Comment.class);
                    listOfComments.add(comment);
                    Log.d("FirebaseDataUnitTest", "Comment information: " + comment.toString());
                    // Toast.makeText(getApplication(), "shop: " + shop.getName(), Toast.LENGTH_SHORT).show();
                }

                Assert.assertEquals(4, listOfComments.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
